import time

import numpy as np
from bec_lib.device import DeviceBase
from bec_server.scan_server.scans import Acquire, AsyncFlyScanBase

class AcquireDark(Acquire):
    scan_name = "acquire_dark"
    required_kwargs = ["exp_burst"]
    gui_config = {"Acquisition parameters": ["exp_burst"]}

    def __init__(self, exp_burst: int, **kwargs):
        """
        Acquire dark images. This scan is used to acquire dark images. Dark images are images taken with the shutter
        closed and no beam on the sample. Dark images are used to correct the data images for dark current.

        Args:

        exp_burst : int
            Number of dark images to acquire (no default)
        exp_time : float, optional
            Exposure time [ms]. If not specified, the currently configured value on the camera will be used
        exp_period : float, optional
            Exposure period [ms]
        image_width : int, optional
            ROI size in the x-direction [pixels]
        image_height : int, optional
            ROI size in the y-direction [pixels]
        acq_mode : str, optional
            Predefined acquisition mode (default= 'default')
        file_path : str, optional
            File path for standard daq

        Returns:
            ScanReport

        Examples:
            >>> scans.acquire_dark(5)

        """
        super().__init__(**kwargs)
        self.burst_at_each_point = 1 # At each point, how many times I want to individually trigger 
        self.scan_motors = ["eyex"]  # change to the correct shutter device
        #self.shutter = "eyex"  # change to the correct shutter device
        self.dark_shutter_pos = 0 ### change with a variable

    def scan_core(self):
        # close the shutter
        yield from self._move_scan_motors_and_wait(self.dark_shutter_pos)
        #yield from self.stubs.set_and_wait(device=[self.shutter], positions=[0])
        yield from super().scan_core()


class AcquireWhite(Acquire):
    scan_name = "acquire_white"
    required_kwargs = ["exp_burst", "sample_position_out", "sample_angle_out"]
    gui_config = {"Acquisition parameters": ["exp_burst"]}

    def __init__(self, exp_burst: int, sample_position_out: float, sample_angle_out: float, **kwargs):
        """
        Acquire flat field images. This scan is used to acquire flat field images. The flat field image is an image taken
        with the shutter open but the sample out of the beam. Flat field images are used to correct the data images for
        non-uniformity in the detector.

        Args:
        exp_burst : int
            Number of flat field images to acquire (no default)
        sample_position_out : float
            Position to move the sample stage to position the sample out of beam and take flat field images
        sample_angle_out : float
            Angular position where to take the flat field images
        exp_time : float, optional
            Exposure time [ms]. If not specified, the currently configured value on the camera will be used
        exp_period : float, optional
            Exposure period [ms]. If not specified, the currently configured value on the camera will be used
        image_width : int, optional
            ROI size in the x-direction [pixels]. If not specified, the currently configured value on the camera will be used
        image_height : int, optional
            ROI size in the y-direction [pixels]. If not specified, the currently configured value on the camera will be used
        acq_mode : str, optional
            Predefined acquisition mode (default= 'default')
        file_path : str, optional
            File path for standard daq

        Returns:
            ScanReport

        Examples:
            >>> scans.acquire_white(5, 20)

        """
        super().__init__(**kwargs)
        self.burst_at_each_point = 1
        self.sample_position_out = sample_position_out
        self.sample_angle_out = sample_angle_out

        self.scan_motors = ["eyex", "eyez", "es1_roty"]  # change to the correct shutter device
        self.dark_shutter_pos_out = 1 ### change with a variable
        self.dark_shutter_pos_in = 0 ### change with a variable


    def scan_core(self):
        # open the shutter and move the sample stage to the out position
        self.scan_motors = ["eyez", "es1_roty"]  # change to the correct shutter device
        yield from self._move_scan_motors_and_wait([self.sample_position_out, self.sample_angle_out])
        self.scan_motors = ["eyex"]  # change to the correct shutter device
        yield from self._move_scan_motors_and_wait([self.dark_shutter_pos_out])
        # TODO add opening of fast shutter
        yield from super().scan_core()
        
        # TODO add closing of fast shutter
        yield from self._move_scan_motors_and_wait([self.dark_shutter_pos_in])

class AcquireProjectins(Acquire):
    scan_name = "acquire_projections"
    required_kwargs = ["exp_burst", "sample_position_in", "start_position", "angular_range"]
    gui_config = {"Acquisition parameters": ["exp_burst"]}

    def __init__(self, 
                 exp_burst: int,
                 sample_position_in: float,
                 start_position: float,
                 angular_range: float,
                 **kwargs):
        """
        Acquire projection images. 

        Args:
        exp_burst : int
            Number of flat field images to acquire (no default)
        sample_position_in : float
            Position to move the sample stage to position the sample in the beam
        start_position : float
            Angular start position for the scan
        angular_range : float
            Angular range
        exp_time : float, optional
            Exposure time [ms]. If not specified, the currently configured value on the camera will be used
        exp_period : float, optional
            Exposure period [ms]. If not specified, the currently configured value on the camera will be used
        image_width : int, optional
            ROI size in the x-direction [pixels]. If not specified, the currently configured value on the camera will be used
        image_height : int, optional
            ROI size in the y-direction [pixels]. If not specified, the currently configured value on the camera will be used
        acq_mode : str, optional
            Predefined acquisition mode (default= 'default')
        file_path : str, optional
            File path for standard daq

        Returns:
            ScanReport

        Examples:
            >>> scans.acquire_white(5, 20)

        """
        super().__init__(**kwargs)
        self.burst_at_each_point = 1
        self.sample_position_in = sample_position_in
        self.start_position = start_position
        self.angular_range = angular_range

        self.scan_motors = ["eyex", "eyez", "es1_roty"]  # change to the correct shutter device
        self.dark_shutter_pos_out = 1 ### change with a variable
        self.dark_shutter_pos_in = 0 ### change with a variable


    def scan_core(self):
        # open the shutter and move the sample stage to the out position
        self.scan_motors = ["eyez", "es1_roty"]  # change to the correct shutter device
        yield from self._move_scan_motors_and_wait([self.sample_position_out, self.sample_angle_out])
        self.scan_motors = ["eyex"]  # change to the correct shutter device
        yield from self._move_scan_motors_and_wait([self.dark_shutter_pos_out])
        # TODO add opening of fast shutter
        yield from super().scan_core()
        
        # TODO add closing of fast shutter
        yield from self._move_scan_motors_and_wait([self.dark_shutter_pos_in])


class AcquireRefs(Acquire):
    scan_name = "acquire_refs"
    required_kwargs = []
    gui_config = {}

    def __init__(
        self,
        num_darks: int = 0,
        num_flats: int = 0,
        sample_angle_out: float = 0,
        sample_position_in: float = 0,
        sample_position_out: float = 5000,
        file_prefix_dark: str = 'tmp_dark',
        file_prefix_white: str = 'tmp_white',
        exp_time: float = 0,
        exp_period: float = 0,
        image_width: int = 2016,
        image_height: int = 2016,
        acq_mode: str = 'default',
        file_path: str = 'tmp',
        nr_writers: int = 2,
        base_path: str = 'tmp',
        **kwargs
    ):
        """
        Acquire reference images (darks + whites) and return to beam position.
        
        Reference images are acquired automatically in an optimized sequence and
        the sample is returned to the sample_in_position afterwards.

        Args:
        num_darks : int , optional
            Number of dark field images to acquire
        num_flats : int , optional
            Number of white field images to acquire
        sample_position_in : float , optional
            Sample stage X position for sample in beam [um]
        sample_position_out : float ,optional
            Sample stage X position for sample out of the beam [um]
        sample_angle_out : float , optional
            Angular position where to take the flat field images
        exp_time : float, optional
            Exposure time [ms]. If not specified, the currently configured value on the camera will be used
        exp_period : float, optional
            Exposure period [ms]. If not specified, the currently configured value on the camera will be used
        image_width : int, optional
            ROI size in the x-direction [pixels]. If not specified, the currently configured value on the camera will be used
        image_height : int, optional
            ROI size in the y-direction [pixels]. If not specified, the currently configured value on the camera will be used
        acq_mode : str, optional
            Predefined acquisition mode (default= 'default')
        file_path : str, optional
            File path for standard daq

        Returns:
            ScanReport

        Examples:
            >>> scans.acquire_refs(sample_angle_out=90, sample_position_in=10, num_darks=5, num_flats=5, exp_time=0.1)

        """
        super().__init__(**kwargs)
        self.sample_position_in = sample_position_in
        self.sample_position_out = sample_position_out
        self.sample_angle_out = sample_angle_out
        self.num_darks = num_darks
        self.num_flats = num_flats
        self.file_prefix_dark = file_prefix_dark
        self.file_prefix_white = file_prefix_white
        self.exp_time = exp_time
        self.exp_period = exp_period
        self.image_width = image_width
        self.image_height = image_height
        self.acq_mode = acq_mode
        self.file_path = file_path
        self.nr_writers = nr_writers
        self.base_path = base_path

    def scan_core(self):

        ## TODO move sample in position and do not wait
        ## TODO move angle in position and do not wait
        if self.num_darks:
            self.connector.send_client_info(
                f"Acquiring {self.num_darks} dark images",
                show_asap=True,
                rid=self.metadata.get("RID"),
            )
            darks = AcquireDark(
                exp_burst=self.num_darks,
                file_prefix=self.file_prefix_dark,
                device_manager=self.device_manager,
                metadata=self.metadata
            )
            yield from darks.scan_core()
            self.point_id = darks.point_id

        if self.num_flats:
            self.connector.send_client_info(
                f"Acquiring {self.num_flats} flat field images",
                show_asap=True,
                rid=self.metadata.get("RID"),
            )
            flats = AcquireWhite(
                exp_burst=self.num_flats,
                sample_position_out=self.sample_position_out,
                sample_angle_out=self.sample_angle_out,
                file_prefix=self.file_prefix_white,
                device_manager=self.device_manager,
                metadata=self.metadata,
            )
            flats.point_id = self.point_id
            yield from flats.scan_core()
            self.point_id = flats.point_id
        ## TODO move sample in beam and do not wait
        ## TODO move rotation to angle and do not wait


class TutorialFlyScanContLine(AsyncFlyScanBase):
    scan_name = "tutorial_cont_line_fly_scan"
    required_kwargs = ["motor"]
    gui_config = {"Acquisition parameters": ["motor"]}

    def __init__(
        self,
        motor: DeviceBase,
        start: float,
        stop: float,
        sample_in: float,
        sample_out: float,
        num_darks: int = 0,
        num_flats: int = 0,
        exp_time: float = 0,
        relative: bool = False,
        **kwargs,
    ):
        """
        A continuous line fly scan. Use this scan if you want to move a motor continuously from start to stop position whilst
        acquiring data as fast as possible (respecting the exposure time). The scan will stop automatically when the motor
        reaches the end position.

        Args:
            motor (DeviceBase): motor to move continuously from start to stop position
            start (float): start position
            stop (float): stop position
            sample_in (float): position to move the sample stage to take the data image
            sample_out (float): position to move the sample stage to take the flat field image
            exp_time (float): exposure time in seconds. Default is 0.
            relative (bool): if True, the motor will be moved relative to its current position. Default is False.

        Returns:
            ScanReport

        Examples:
            >>> scans.tutorial_cont_line_fly_scan(dev.sam_rot, 0, 360, 20, -200, num_darks=5, num_flats=5, exp_time=0.1)

        """
        super().__init__(**kwargs)
        self.motor = motor
        self.start = start
        self.stop = stop
        self.sample_in = sample_in
        self.sample_out = sample_out
        self.num_darks = num_darks
        self.num_flats = num_flats
        self.exp_time = exp_time
        self.relative = relative
        self.sample_stage = "samy"  # change to the correct sample stage device
        self.shutter = "hx"  # change to the correct shutter device

    def prepare_positions(self):
        self.positions = np.array([[self.start], [self.stop]])
        self.num_pos = None
        yield from self._set_position_offset()

    def scan_core(self):
        # move the motor to the start position

        if self.num_darks:
            self.connector.send_client_info(
                f"Acquiring {self.num_darks} dark images",
                show_asap=True,
                rid=self.metadata.get("RID"),
            )
            darks = AcquireDark(
                num=self.num_darks,
                device_manager=self.device_manager,
                metadata=self.metadata,
                exp_time=self.exp_time,
            )
            yield from darks.scan_core()
            self.point_id = darks.point_id

        if self.num_flats:
            self.connector.send_client_info(
                f"Acquiring {self.num_flats} flat field images",
                show_asap=True,
                rid=self.metadata.get("RID"),
            )
            flats = AcquireWhite(
                num=self.num_flats,
                exp_time=self.exp_time,
                out_position=self.sample_out,
                device_manager=self.device_manager,
                metadata=self.metadata,
            )
            flats.point_id = self.point_id
            yield from flats.scan_core()
            self.point_id = flats.point_id

        # move to in position and open the shutter
        yield from self.stubs.set_and_wait(
            device=[self.sample_stage, self.shutter], positions=[self.sample_in, 1]
        )

        yield from self.stubs.set_and_wait(device=[self.motor], positions=self.positions[0])

        # start the flyer
        flyer_request = yield from self.stubs.set_with_response(
            device=self.motor, value=self.positions[1][0]
        )

        self.connector.send_client_info(
            "Starting the scan", show_asap=True, rid=self.metadata.get("RID")
        )
        # send a trigger
        yield from self.stubs.trigger(group="trigger", point_id=self.point_id)
        while True:
            # read the data
            yield from self.stubs.read_and_wait(
                group="primary", wait_group="readout_primary", point_id=self.point_id
            )
            time.sleep(1)

            if self.stubs.request_is_completed(flyer_request):
                # stop the scan if the motor has reached the stop position
                break

            # increase the point id
            self.point_id += 1

    def finalize(self):
        yield from super().finalize()
        self.num_pos = self.point_id + 1
