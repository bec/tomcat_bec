import numpy as np

from bec_lib.device import DeviceBase
from bec_server.scan_server.scans import AsyncFlyScanBase


class TutorialFlyScanContLine(AsyncFlyScanBase):
    scan_name = "tutorial_cont_line_fly_scan"

    def __init__(  # Input arguments
        self,
        motor: DeviceBase,
        start: float,
        stop: float,
        exp_time: float = 0,
        relative: bool = False,
        **kwargs,   ### For more metadata
    ):
        """
        A continuous line fly scan. Use this scan if you want to move a motor continuously from start to stop position whilst
        acquiring data as fast as possible (respecting the exposure time). The scan will stop automatically when the motor
        reaches the end position.

        Args:
            motor (DeviceBase): motor to move continuously from start to stop position
            start (float): start position
            stop (float): stop position
            exp_time (float): exposure time in seconds. Default is 0.
            relative (bool): if True, the motor will be moved relative to its current position. Default is False.

        Returns:
            ScanReport

        Examples:
            >>> scans.tutorial_cont_line_fly_scan(dev.sam_rot, 0, 180, exp_time=0.1)

        """
        super().__init__(**kwargs)   # I need to just give weiter these
        self.motor = motor   # Can filter the config for the active rotation stage
        self.start = start
        self.stop = stop
        self.exp_time = exp_time
        self.relative = relative

    def prepare_positions(self):
        self.positions = np.array([[self.start], [self.stop]])
        self.num_pos = None   # If I do not want to read out anything, this could be 1 -> If it is known it is easy for the progress bar
        yield from self._set_position_offset()

    def scan_core(self):
        # move the motor to the start position = move command in the scan world
        yield from self.stubs.set_and_wait(device=[self.motor], positions=self.positions[0])   # go to zero

        # start the flyer -> start rotation
        flyer_request = yield from self.stubs.set_with_response(device=self.motor, value=self.positions[1][0])


        while True:  # loop for what happens during the scan
            # send a trigger
            yield from self.stubs.trigger(group="trigger", point_id=self.point_id)
            # wait for the trigger to complete
            yield from self.stubs.wait(
                wait_type="trigger", group="trigger", wait_time=self.exp_time
            )
            # read the data   -> asynchrone devices large detector (bekomme die daten erst am ende vom scan )
            yield from self.stubs.read_and_wait(
                group="primary", wait_group="readout_primary", point_id=self.point_id
            )

            if self.stubs.request_is_completed(flyer_request):
                # stop the scan if the motor has reached the stop position
                break

            # increase the point id
            self.point_id += 1

    def finalize(self):   # finalize erweitern
        yield from super().finalize()
        self.num_pos = self.point_id + 1