# -*- coding: utf-8 -*-
""" Tomcat scan base class examples

A collection of example scan base classes using Automation1 rotation stage,
GigaFrost camera and the StandardDAQ pipeline. 

NOTE: As an explicit request from Tomcat, all devices must be freely
interchangeable, including the simultaneous use of multiple devices. Therefore
the devices were prepared in such a way, that the scans need to address
devices.

Created on Mon Sep 16 16:45:11 2024

@author: mohacsi_i
"""
import jinja2
import os
import time

from bec_lib import bec_logger
from bec_server.scan_server.scans import AsyncFlyScanBase, ScanBase, ScanArgType

logger = bec_logger.logger


class TomcatStepScan(ScanBase):
    """Simple software step scan for Tomcat

    Example class for simple BEC-based step scan using the low-level API. It's just a standard
    'line_scan' with the only difference that overrides burst behavior to use camera burst instead
    of individual software triggers. 

    NOTE: As decided by Tomcat, the scans should not manage the scope of devices
        - All enabled devices are expected to be configured for acquisition by the end of stage
            - Some devices can configure themselves from mandatory scan parameters (steps, burst)
            - Other devices can be optionally configured by keyword arguments
            - Devices will try to stage using whatever was set on them before

    Example:
    --------
        >>> scans.tomcatstepscan(scan_start=-25, scan_end=155, steps=180, exp_time=0.005, exp_burst=5)

    Common keyword arguments:
    -------------------------
    image_width :  int
    image_height : int
    ddc_trigger : str
    """

    scan_name = "tomcatstepscan"
    scan_type = "step"
    required_kwargs = ["scan_start", "scan_end", "steps"]
    gui_config = {
        "Movement parameters": ["steps"],
        "Acquisition parameters": ["exp_time", "exp_burst"],
    }

    def update_scan_motors(self):
        self.scan_motors = ["es1_roty"]

    def _get_scan_motors(self):
        self.scan_motors = ["es1_roty"]

    def __init__(
        self,
        scan_start: float,
        scan_end: float,
        steps: int,
        exp_time: float=0.005,
        settling_time: float=0.2,
        exp_burst: int=1,
        **kwargs,
    ):
        # Converting generic kwargs to tomcat device configuration parameters
        super().__init__(
            exp_time=exp_time,
            settling_time=settling_time,
            burst_at_each_point=1,
            optim_trajectory=None,
            **kwargs,
        )

        # For position calculation
        self.motor = "es1_roty"
        self.scan_start = scan_start
        self.scan_end = scan_end
        self.scan_steps = steps
        self.scan_stepsize = (scan_end - scan_start) / steps

    def _calculate_positions(self) -> None:
        """Pre-calculate scan positions"""
        for ii in range(self.scan_steps + 1):
            self.positions.append(self.scan_start + ii * self.scan_stepsize)

    def _at_each_point(self, ind=None, pos=None):
        """ Overriden at_each_point, using detector burst instaead of manual triggering"""
        yield from self._move_scan_motors_and_wait(pos)
        time.sleep(self.settling_time)

        trigger_time = 0.001*self.exp_time * self.burst_at_each_point
        yield from self.stubs.trigger(min_wait=trigger_time)
        # yield from self.stubs.trigger(group='trigger', point_id=self.point_id)
        # time.sleep(trigger_time)

        yield from self.stubs.read(group="monitored", point_id=self.point_id)
        # yield from self.stubs.read(group="monitored", point_id=self.point_id, wait_group=None)
        self.point_id += 1


class TomcatSnapNStep(AsyncFlyScanBase):
    """Simple software step scan forTomcat

    Example class for simple BEC-based step scans using the low-level API. All it does is
    translate conventional kwargs to tomcat specific naming scheme.

    Example
    -------
        >>> scans.tomcatsnapnstepscan(scan_start=-25, scan_end=155, steps=180, exp_time=0.005, exp_burst=5)
    """

    scan_name = "tomcatsnapnstepscan"
    scan_type = "scripted"
    # arg_input = {"camera" : ScanArgType.DEVICE, 
    #              "exp_time" : ScanArgType.FLOAT}
    # arg_bundle_size= {"bundle": len(arg_input), "min": 1, "max": None}
    required_kwargs = ["scan_start", "scan_end", "steps"]
    gui_config = {
        "Movement parameters": ["steps"],
        "Acquisition parameters": ["exp_time", "exp_burst"],
    }

    def _get_scan_motors(self):
        self.scan_motors = ["es1_roty"]

    def __init__(
        self,
        scan_start: float,
        scan_end: float,
        steps: int,
        exp_time:float=0.005,
        settling_time:float=0.2,
        exp_burst:int=1,
        sync:str="event",
        **kwargs,
    ):
        # For substitution calculation
        self.scan_start = scan_start
        self.scan_end = scan_end
        self.scan_steps = steps
        self.scan_stepsize = (scan_end - scan_start) / steps
        self.scan_ntotal = exp_burst * (steps + 1)
        self.exp_time = exp_time
        self.exp_burst = exp_burst
        self.settling_time = settling_time
        self.scan_sync = sync

        # General device configuration
        kwargs["parameter"]["kwargs"]["acq_mode"] = "ext_enable"

        # Used for Aeroscript file substitutions for the task interface
        filename = "AerotechSnapAndStepTemplate.ascript"
        filesubs = self.get_filesubs()
        filetext = self.render_file(filename, filesubs)
        kwargs["parameter"]["kwargs"]["script_text"] = filetext
        kwargs["parameter"]["kwargs"]["script_file"] = "bec.ascript"

        super().__init__(
            exp_time=exp_time,
            settling_time=settling_time,
            burst_at_each_point=1,
            optim_trajectory=None,
            **kwargs,
        )

    def get_filesubs(self) -> dict:
        """Generate jinja template substitutions."""
        p_modes = {
            "pso": "PsoOutput",
            "event": "PsoEvent",
            "inp0": "HighSpeedInput0RisingEdge",
            "inp1": "HighSpeedInput1RisingEdge",
        }
        filesubs = {
            "startpos": self.scan_start,
            "stepsize": self.scan_stepsize,
            "numsteps": self.scan_steps,
            "exptime": 2 * self.exp_time * self.exp_burst,
            "settling": self.settling_time,
            "psotrigger": p_modes[self.scan_sync],
            "npoints": self.scan_ntotal,
        }
        return filesubs

    def render_file(self, filename, filesubs):
        """Prepare action: render AeroScript file"""
        # Load the test file
        filename = os.path.join(os.path.dirname(__file__), "../devices/aerotech/" + filename)
        logger.info(f"Attempting to load file {filename}")
        with open(filename) as f:
            templatetext = f.read()

        # Substitute jinja template
        tm = jinja2.Template(templatetext)
        filetext = tm.render(scan=filesubs)
        return filetext

    def scan_core(self):
        """The actual scan routine"""
        print("TOMCAT Running scripted scan (via Jinjad AeroScript)")
        t_start = time.time()

        # Complete
        # FIXME: this will swallow errors
        # yield from self.stubs.complete(device="es1_tasks")
        st = yield from self.stubs.send_rpc_and_wait("es1_tasks", "complete")
        # st.wait()
        task_states = yield from self.stubs.send_rpc_and_wait("es1_tasks", "taskStates.get")
        if task_states[4] == 8:
            raise RuntimeError(f"Task {4} finished in ERROR state")

        t_end = time.time()
        t_elapsed = t_end - t_start
        logger.warning(f"Elapsed scan time: {t_elapsed}")
        time.sleep(0.5)

        # Collect
        # if self.daqname is not None and self.daqmode=="collect":
        #     print("TOMCAT Collecting scripted scan results (from EPICS)")
        #     positions = yield from self.stubs.send_rpc_and_wait(self.daqname, "collect")
        #     logger.info(f"Finished scan with collected positions: {positions}")

    def cleanup(self):
        """Set scan progress to 1 to finish the scan"""
        self.num_pos = 1
        return super().cleanup()


class TomcatSimpleSequence(AsyncFlyScanBase):
    """Simple software step scan forTomcat

    Example class for simple AeroScript-based step scan using the low-level API. All it does is
    translate meaningful kwargs to device specific naming scheme.

    Example
    -------
        >>> scans.tomcatsimplesequencescan(33, 180, 180, exp_time=0.005, exp_burst=1800, repeats=10)
    """

    scan_name = "tomcatsimplesequencescan"
    scan_type = "scripted"
    scan_report_hint = "table"
    required_kwargs = ["scan_start", "gate_high", "gate_low"]
    gui_config = {
        "Movement parameters": ["scan_start", "repeats", "repmode"],
        "Acquisition parameters": [
            "gate_high",
            "gate_low",
            "exp_time",
            "exp_burst",
            "sync",
        ],
    }

    def _get_scan_motors(self):
        self.scan_motors = ["es1_roty"]

    def __init__(
        self,
        scan_start: float,
        gate_high: float,
        gate_low: float,
        repeats: int = 1,
        repmode: str = "PosNeg",
        exp_time: float = 0.005,
        exp_burst: float = 180,
        sync: str = "pso",
        **kwargs,
    ):
        # Auto-setup configuration parameters from input
        self.scan_start = scan_start
        self.gate_high = gate_high
        self.gate_low = gate_low
        self.scan_repnum = repeats
        self.scan_repmode = repmode.upper()
        self.exp_time = exp_time
        self.exp_burst = exp_burst
        self.scan_sync = sync

        # Synthetic values
        self.scan_ntotal = exp_burst * repeats
        self.scan_velocity = gate_high / (exp_time * exp_burst)
        self.scan_acceleration = 500
        self.scan_safedistance = 10
        self.scan_accdistance = (
            self.scan_safedistance
            + 0.5 * self.scan_velocity * self.scan_velocity / self.scan_acceleration
        )

        if self.scan_repmode in ("POS", "NEG"):
            self.scan_range = repeats * (gate_high + gate_low)
        elif self.scan_repmode in ("POSNEG", "NEGPOS"):
            self.scan_range = gate_high + gate_low
        else:
            raise RuntimeError(f"Unsupported repetition mode: {self.scan_repmode}")

        # General device configuration
        kwargs["parameter"]["kwargs"]["acq_mode"] = "ext_enable"

        # Used for Aeroscript file substitutions for the task interface
        filename = "AerotechSimpleSequenceTemplate.ascript"
        filesubs = self.get_filesubs()
        filetext = self.render_file(filename, filesubs)
        kwargs["parameter"]["kwargs"]["script_text"] = filetext
        kwargs["parameter"]["kwargs"]["script_file"] = "bec.ascript"

        super().__init__(
            exp_time=exp_time,
            settling_time=0.5,
            relative=False,
            burst_at_each_point=1,
            optim_trajectory=None,
            **kwargs,
        )

    def render_file(self, filename, filesubs):
        """Prepare action: render AeroScript file"""
        # Load the test file
        filename = os.path.join(os.path.dirname(__file__), "../devices/aerotech/" + filename)
        logger.info(f"Attempting to load file {filename}")
        with open(filename) as f:
            templatetext = f.read()

        # Substitute jinja template
        tm = jinja2.Template(templatetext)
        filetext = tm.render(scan=filesubs)
        return filetext

    def scan_core(self):
        """The actual scan routine"""
        print("TOMCAT Running scripted scan (via Jinjad AeroScript)")
        t_start = time.time()

        # Complete
        # FIXME: this will swallow errors
        # yield from self.stubs.complete(device="es1_tasks")
        st = yield from self.stubs.send_rpc_and_wait("es1_tasks", "complete")
        # st.wait()
        task_states = yield from self.stubs.send_rpc_and_wait("es1_tasks", "taskStates.get")
        if task_states[4] == 8:
            raise RuntimeError(f"Task {4} finished in ERROR state")

        t_end = time.time()
        t_elapsed = t_end - t_start
        logger.warning(f"Elapsed scan time: {t_elapsed}")
        time.sleep(0.5)

        # Collect
        # if self.daqname is not None and self.daqmode=="collect":
        #     print("TOMCAT Collecting scripted scan results (from EPICS)")
        #     positions = yield from self.stubs.send_rpc_and_wait(self.daqname, "collect")
        #     logger.info(f"Finished scan with collected positions: {positions}")

    def get_filesubs(self):
        """Generate jinja template substitutions."""
        # Aerotech DDC settings: Internal event trigger: PsoEvent = 1
        p_modes = {
            "pso": "PsoOutput",
            "event": "PsoEvent",
            "inp0": "HighSpeedInput0RisingEdge",
            "inp1": "HighSpeedInput1RisingEdge",
        }
        filesubs = {
            "startpos": self.scan_start,
            "scanrange": self.scan_range,
            "nrepeat": self.scan_repnum,
            "scanvel": self.scan_velocity,
            "scanacc": self.scan_acceleration,
            "accdist": self.scan_accdistance,
            "npoints": self.scan_ntotal,
            "scandir": self.scan_repmode,
            "psotrigger": p_modes[self.scan_sync],
        }

        # Fill PSO vectors according to scan direction
        # NOTE: Distance counter is bidirectional
        pso_bounds_pos = []
        pso_bounds_neg = []
        if self.scan_repmode in ("pos", "Pos", "POS", "neg", "Neg", "NEG"):
            pso_bounds_pos.append(self.scan_accdistance)
            for ii in range(self.scan_repnum):
                pso_bounds_pos += [self.gate_high, self.gate_low]
            pso_bounds_pos = pso_bounds_pos[:-1]
        if self.scan_repmode in ("posneg", "PosNeg", "POSNEG", "negpos", "NegPos", "NEGPOS"):
            pso_bounds_pos = [self.scan_accdistance, self.gate_high]
            pso_bounds_neg = [self.scan_accdistance + self.gate_low, self.gate_high]

        filesubs["psoBoundsPos"] = pso_bounds_pos
        filesubs["psoBoundsNeg"] = pso_bounds_neg
        filesubs["psoBoundsPosSize"] = len(pso_bounds_pos)
        filesubs["psoBoundsNegSize"] = len(pso_bounds_neg)
        return filesubs

    def cleanup(self):
        """Set scan progress to 1 to finish the scan"""
        self.num_pos = 1
        return super().cleanup()
