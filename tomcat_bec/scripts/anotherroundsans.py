


""" Demo scans for Tomcat at the microXAS test bench
"""


# def bl_check_beam():
#     """Checks beamline status"""
#     motor_enabled = bool(dev.es1_roty.motor_enable.get())
#     result = motor_enabled
#     return result


def dev_disable_all():
    """Disable all devices """
    for k in dev:
        dev[k].enabled = False


def cam_select(camera: str):
    """Select the active camera pipeline"""
    if isinstance(camera, str):
        if camera in ("gf", "gf2", "gigafrost"):
            dev.gfcam.enabled = True
            dev.gfdaq.enabled = True
            dev.daq_stream0.enabled = True
            dev.pcocam.enabled = False
            dev.pcodaq.enabled = False
            dev.pco_stream0.enabled = False
        if camera in ("pco", "pco.edge", "pcoedge"):
            dev.gfcam.enabled = False
            dev.gfdaq.enabled = False
            dev.daq_stream0.enabled = False
            dev.pcocam.enabled = True
            dev.pcodaq.enabled = True
            dev.pco_stream0.enabled = True



def anotherstepscan(
    scan_start,
    scan_end,
    steps,
    exp_time=0.005,
    exp_burst=5,
    settling_time=0,
    camera=None,
    image_width=None,
    image_height=None,
    sync="inp1",
    **kwargs
):
    """Demo step scan with GigaFrost

    This is a small BEC user-space demo step scan at the microXAS testbench
    using the gigafrost in software triggering mode. It tries to be a
    standard BEC scan, while still setting up the environment.

    Example:
    --------
    demostepscan(scan_start=-32, scan_end=148, steps=180, exp_time=0.005, exp_burst=5)
    """
    # Check beamline status before scan
    if not bl_check_beam():
        raise RuntimeError("Beamline is not in ready state")
    # Enable the correct camera before scan
    cam_select(camera)
    # This scan uses software triggering
    if isinstance(camera, str):
        if camera in ("gf", "gf2", "gigafrost"):
            dev.gfcam.software_trigger = True
        if camera in ("pco", "pco.edge", "pcoedge"):
            dev.pcocam.software_trigger = True

    # Disable aerotech controller
    # dev.es1_tasks.enabled = False
    # dev.es1_psod.enabled = False
    # dev.es1_ddaq.enabled = True
    # Enable scan motor
    dev.es1_roty.enabled = True



    print("Handing over to 'scans.tomcatstepscan'")
    try:
        scans.tomcatstepscan(
            scan_start=scan_start,
            scan_end=scan_end,
            steps=steps,
            exp_time=exp_time,
            exp_burst=exp_burst,
            relative=False,
            image_width=image_width,
            image_height=image_height,
            settling_time=settling_time,
            sync=sync,
            **kwargs
        )
    finally:
        # if isinstance(camera, str):
        #     if camera in ("gf", "gf2", "gigafrost"):
        #         dev.gfcam.software_trigger = False
        #     if camera in ("pco", "pco.edge", "pcoedge"):
        #         dev.pcocam.software_trigger = False
        pass




def anothersequencescan(
    scan_start,
    gate_high,
    gate_low,
    repeats=1,
    repmode="PosNeg",
    exp_time=0.005,
    exp_burst=180,
    camera=None,
    image_width=None,
    image_height=None,
    sync="pso",
    **kwargs
):
    """Demo sequence scan with GigaFrost

    This is a small BEC user-space sequence scan at the microXAS testbench
    triggering a customized scripted scan on the controller. The scan uses
    a pre-written custom low-level sequence scan, so it is really minimal.
    It also ensures that all arguments are passed on as kwargs.

    NOTE: It calls the AeroScript template version.

    Example:
    --------
        >>> anothersequencescan(33, 180, 180, exp_time=0.005, exp_frames=1800, repeats=10)
    """
    # Check beamline status before scan
    if not bl_check_beam():
        raise RuntimeError("Beamline is not in ready state")
    # Enable the correct camera before scan
    cam_select(camera)
    # Enabling aerotech controller
    dev.es1_tasks.enabled = True
    dev.es1_psod.enabled = False
    dev.es1_ddaq.enabled = True

    print("Handing over to 'scans.sequencescan'")
    scans.tomcatsimplesequencescan(
        scan_start=scan_start,
        gate_high=gate_high,
        gate_low=gate_low,
        repeats=repeats,
        repmode=repmode,        
        exp_time=exp_time,
        exp_burst=exp_burst,
        image_width=image_width,
        image_height=image_height,
        sync=sync,
        **kwargs
    )




def anothersnapnstepscan(
    scan_start,
    scan_end,
    steps,
    exp_time=0.005,
    exp_burst=180,
    camera=None,
    image_width=None,
    image_height=None,
    settling_time=0.1,
    sync="pso",
    **kwargs
):
    """Demo snapnstep scan with GigaFrost

    This is a small BEC user-space scan at the microXAS testbench that handles
    enabling/disabling devices before handing over to the low level scan. It
    also ensures that all arguments are passed on as kwargs.
    The actual logic is implemented in a templated  AeroScript file.

    Example:
    --------
        >>> anothersnapnstepscan(33, 180, 180, exp_time=0.005, exp_frames=1800, repeats=10)
    """
    # Check beamline status before scan
    if not bl_check_beam():
        raise RuntimeError("Beamline is not in ready state")
    # Enable the correct camera before scan
    cam_select(camera)
    # Enabling aerotech controller
    dev.es1_tasks.enabled = True
    dev.es1_psod.enabled = False
    dev.es1_ddaq.enabled = True

    print("Handing over to 'scans.tomcatsnapnstepscan'")
    scans.tomcatsnapnstepscan(
        scan_start=scan_start,
        scan_end=scan_end,
        steps=steps,
        exp_time=exp_time,
        exp_burst=exp_burst,
        image_width=image_width,
        image_height=image_height,
        settling_time=settling_time,
        sync=sync,
        **kwargs
    )







def ascan(motor, scan_start, scan_end, steps, exp_time, datasource, visual=True, **kwargs):
    """Demo step scan with plotting

    This is a simple user-space demo step scan with automatic plorring and fitting via the BEC.
    It's mostly a standard BEC scan, just adds GUI setup and fits a hardcoded LlinearModel at
    the end (other models are more picky on the initial parameters).

    Example:
    --------
    ascan(dev.dccm_energy, 12,13, steps=21, exp_time=0.1, datasource=dev.dccm_xbpm)
    """
    # Dummy method to check beamline status
    if not bl_check_beam():
        raise RuntimeError("Beamline is not in ready state")

    if visual:
        # Get or create scan specific window
        window = None
        for _, val in bec.gui.windows.items():
            if val.title == "CurrentScan":
                window = val.widget
                window.clear_all()
        if window is None:
            window = bec.gui.new("CurrentScan")

        # Draw a simploe plot in the window
        dock = window.add_dock(f"ScanDisplay {motor}")
        plt1 = dock.add_widget("BECWaveformWidget")
        plt1.plot(x_name=motor, y_name=datasource)
        plt1.set_x_label(motor)
        plt1.set_y_label(datasource)
        plt1.add_dap(motor, datasource, dap="LinearModel")
        window.show()

    print("Handing over to 'scans.line_scan'")
    if "relative" in kwargs:
        del kwargs["relative"]
    s = scans.line_scan(
        motor, scan_start, scan_end, steps=steps, exp_time=exp_time, relative=False, **kwargs
    )

    if visual:
        fit = plt1.get_dap_params()
    else:
        fit = bec.dap.LinearModel.fit(s, motor.name, motor.name, datasource.name, datasource.name)

    # TODO: Move to fitted value... like center, peak, edge, etc...

    return s, fit