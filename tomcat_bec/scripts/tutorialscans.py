

def tutorialdarks(num, exp_time=5, roix=2016, roiy=2016):
    """ Dutorial dark scan

    This is a small BEC user-space scan that sets up the gigafrost and calls
    the low-level dark collection scan.

    Example:
    --------
    tutorialdarks(num=100, exp_time=5)
    """
    # Configure gigafrost
    cfg = {
        "ntotal": num, "nimages": 1, 
        "exposure": exp_time, "period": 2*exp_time,
        "pixel_width": roix, "pixel_height": roiy
        }
    dev.gfclient.configure(d=cfg)

    dev.gfclient.cam.cfgAcqMode.set(1).wait()
    dev.gfclient.cam.cfgEnableExt.set(0).wait()
    dev.gfclient.cam.cfgEnableSoft.set(0).wait()
    dev.gfclient.cam.cfgEnableAlways.set(1).wait()

    dev.gfclient.cam.cfgTrigExt.set(0).wait()
    dev.gfclient.cam.cfgTrigSoft.set(0).wait()
    dev.gfclient.cam.cfgTrigTimer.set(0).wait()
    dev.gfclient.cam.cfgTrigAuto.set(1).wait()

    dev.gfclient.cam.cfgExpExt.set(0).wait()
    dev.gfclient.cam.cfgExpSoft.set(0).wait()
    dev.gfclient.cam.cfgExpTimer.set(1).wait()

    dev.gfclient.cam.cfgCntStartBit.set(0).wait()

    # Commit changes to GF
    dev.gfclient.cam.cmdSetParam.set(1).wait()


    print("Handing over to 'scans.acquire_dark")
    scans.acquire_dark(num, exp_time)

