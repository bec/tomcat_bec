import os.path

class Measurement:
    """
    This class provides a standard set of tomographic measurement functions
    that can be used to acquire data at the TOMCAT beamline
    """
    def __init__(self):
        self.sample_name = 'tmp'
        self.data_path = 'disk_test'
        bec.system_config.file_suffix = None
        bec.system_config.file_directory = os.path.join(self.data_path,self.sample_name)

        self.nimages = 1000
        self.nimages_dark = 50
        self.nimages_white = 100

        self.start_angle = 0
        self.sample_angle_out = 0
        self.sample_position_in = 0
        self.sample_position_out = 1

        # To be able to keep what is already set on the camera
        self.exposure_time = None
        self.exposure_period = None
        self.roix = None
        self.roiy = None

        self.get_available_detectors()
        self.get_enabled_detectors()
        if len(self.enabled_detectors) > 1:
            print('WARNING! More than 1 detector enabled')
            self.show_enabled_detectors()
        elif len(self.enabled_detectors) == 0:
            print("WARNING! No detector enabled!!")
            self.show_available_detectors()
        else:
            self.show_enabled_detectors()
            self.det = self.enabled_detectors[0]
            self.device_name = self.enabled_detectors[0].name        
            self.build_filename()
            self.exposure_time = self.det.cfgExposure.get()
            self.exposure_period = self.det.cfgFramerate.get()
            self.roix = self.det.cfgRoiX.get()
            self.roiy = self.det.cfgRoiY.get()


    def build_filename(self, acquisition_type='data'):
        """
        Build and set filepath for bec
        Build filepath and file prefix for stddaq

        acquisition_type : string, optional
            Type of images: a choice between dark, white, or data (default = data)
        """
        
        if acquisition_type != "data" and acquisition_type != "dark" and acquisition_type != "white":
            print("WARNING: chosen acquisition type not permitted! \n")
            print("The chosen acquitisition type as been set to \"data\"!")
            acquisition_type == "data"

        self.scan_sample_name = 'S' + str(bec.queue.next_scan_number) + '_' + self.sample_name
            
        # File path for bec
        bec.system_config.file_directory = os.path.join(self.data_path,self.sample_name,self.scan_sample_name)
        
        # File path for stddaq (for now root folders different between console and stddaq server)
        self.file_path = os.path.join('/data/test/test-beamline',bec.system_config.file_directory, '_device_dat', self.device_name) # _device_dat does not work for now.        
        # A hack for now to create the right permissions
        self.base_path = os.path.join('/data/test/test-beamline',self.data_path)
        self.file_prefix = self.scan_sample_name + '_' + self.device_name + '_' + acquisition_type + '_'

    def configure(self,sample_name=None, data_path=None, exposure_time=None,
                  exposure_period=None, roix=None, roiy=None,nimages=None,
                  nimages_dark=None, nimages_white=None,
                  start_angle=None, sample_angle_out=None,
                  sample_position_in=None, sample_position_out=None):
        """
        Reconfigure the measurement with any number of new parameter
        
        Parameters
        ----------
        sample_name : string, optional
            Name of the sample or measurement. This name will be used to construct
            the name of the measurement directory (default = None)
        data_path : string, optional
            Information used to build the data directory for the measurement 
            (default = None)
        exposure_time : float, optional
            Exposure time [ms] (default = None)
        exposure_period : float, optional
            Exposure period [ms] (default = None)
        roix : int, optional
            ROI size in the x-direction [pixels] (default = None)
        roiy : int, optional
            ROI size in the y-direction [pixels] (default = None)
        nimages : int, optional
            Number of images to acquire (default = None)
        nimages_dark : int, optional
            Number of dark images to acquire (default = None)
        nimages_white : int, optional
            Number of white images to acquire (default = None)
        start_angle : float, optional
            The start angle for the scan [deg] (default = None)
        sample_angle_out : float, optional
            Sample rotation angle for sample out of the beam [deg]
             (default = None)
        sample_position_in : float, optional
            Sample stage X position for sample in beam [um] (default = None)
        sample_position_out : float, optional
            Sample stage X position for sample out of the beam [um]
            (default = None)
         """

        if sample_name != None:
            self.sample_name = sample_name
        if data_path != None:
            self.data_path = data_path
        if nimages != None:
            self.nimages = nimages
        if nimages_dark != None:
            self.nimages_dark = nimages_dark
        if nimages_white != None:
            self.nimages_white = nimages_white
        if exposure_time != None:
            self.exposure_time = exposure_time
        if exposure_period != None:
            self.exposure_period = exposure_period
        if roix != None:
            self.roix = roix
        if roiy != None: 
            self.roiy = roiy
        if start_angle != None:
            self.start_angle = start_angle
        if sample_angle_out != None:
            self.sample_angle_out = sample_angle_out
        if sample_position_in != None:
            self.sample_position_in = sample_position_in
        if sample_position_out != None:
            self.sample_position_out = sample_position_out

        self.build_filename()

    def disable_detector(self, detector_name):
        """
        Disable detector
        """
        
        if detector_name not in self.available_detectors_names:
            print("WARNING! The detector " + str(detector_name + " is not available!"))
            print("You can check the available detectors with the command dev.show_all()")
        
        devices_to_be_disabled = [obj for obj in dev.get_devices_with_tags(detector_name)]
        for i in range(len(devices_to_be_disabled)):
            devices_to_be_disabled[i].enabled = False
        
        self.get_enabled_detectors()

        if len(self.enabled_detectors) > 1:
            print('WARNING! More than 1 detector enabled')
            self.show_enabled_detectors()
        elif len(self.enabled_detectors) == 0:
            print("WARNING! No detector enabled!!")
            self.show_available_detectors()
        else:
            self.show_enabled_detectors()
            self.det = self.enabled_detectors[0]
            self.device_name = self.enabled_detectors[0].name        
            self.build_filename()


    def enable_detector(self, detector_name):
        """
        Enable detector
        """
        
        if detector_name not in self.available_detectors_names:
            print("WARNING! The detector " + str(detector_name + " is not available!"))
            print("You can check the available detectors with the command dev.show_all()")
        
        devices_to_be_enabled = [obj for obj in dev.get_devices_with_tags(detector_name)]
        for i in range(len(devices_to_be_enabled)):
            devices_to_be_enabled[i].enabled = True
        dev.daq_stream1.enabled = False

        self.get_enabled_detectors()

        if len(self.enabled_detectors) > 1:
            print('WARNING! More than 1 detector enabled')
            self.show_enabled_detectors()
        elif len(self.enabled_detectors) == 0:
            print("WARNING! No detector enabled!!")
            self.show_available_detectors()
        else:
            self.show_enabled_detectors()
            self.det = self.enabled_detectors[0]
            self.device_name = self.enabled_detectors[0].name        
            self.build_filename()



    def get_available_detectors(self):
        """
        Check available detectors
        """
        self.available_detectors = [obj for obj in dev.get_devices_with_tags('camera')]

        self.available_detectors_names = []
        for i in range(len(self.available_detectors)):
            self.available_detectors_names.append(self.available_detectors[i].name)
        
    def get_enabled_detectors(self):
        """
        Get enabled detectors
        """
        self.enabled_detectors = [obj for obj in dev.get_devices_with_tags('camera') if obj.enabled]


    def show_available_detectors(self):
        """
        Show available detectors
        """
        print("Available detectors:")
        for i in range(len(self.available_detectors)):
            print(self.available_detectors[i].name + '\tenabled ' + str(self.available_detectors[i].enabled))
    

    def show_enabled_detectors(self):
        """
        List enabled detectors
        """        
        print("Enabled detectors:")
        for i in range(len(self.enabled_detectors)):
            print(self.enabled_detectors[i].name)


    def show_all(self):

        """
        Show configuration

        TODO: make it work for multiple devices
        """

        print("Sample name: " + self.sample_name)
        print("Data path: " + self.data_path)
        print("Number of images: " + str(self.nimages))
        print("Number of darks: " + str(self.nimages_dark))
        print("Number of flats: " + str(self.nimages_white))
        if self.exposure_time == None:
            print("Exposure time: " + str(self.det.cfgExposure.get()))
            self.exposure_time = self.det.cfgExposure.get()
        else:
            print("Exposure time: " + str(self.exposure_time))
        if self.exposure_period == None:
            print("Exposure period: " + str(self.det.cfgFramerate.get()))
            self.exposure_period = self.det.cfgFramerate.get()
        else:   
            print("Exposure period: " + str(self.exposure_period))
        if self.roix == None:
            print("Roix: " + str(self.det.cfgRoiX.get()))
            self.roix = self.det.cfgRoiX.get()
        else:
            print("Roix: " + str(self.roix))
        if self.roiy == None:
            print("Roiy: " + str(self.det.cfgRoiY.get()))
            self.roiy = self.det.cfgRoiY.get()
        else:
            print("Roiy: " + str(self.roiy))
        print("Start angle: " + str(self.start_angle))
        print("Sample angle out: " + str(self.sample_angle_out))
        print("Sample position in: " + str(self.sample_position_in))
        print("Sample position out: " + str(self.sample_position_out))
        
    
    def acquire_darks(self,nimages_dark=None, exposure_time=None, exposure_period=None, 
                      roix=None, roiy=None, acq_mode=None):
        """
        Acquire a set of dark images with shutters closed.

        Parameters
        ----------
        nimages_dark : int, optional
            Number of dark images to acquire (no default)
        exposure_time : float, optional
            Exposure time [ms]. If not specified, the currently configured value on the camera will be used
        exposure_period : float, optional
            Exposure period [ms]
        roix : int, optional
            ROI size in the x-direction [pixels]
        roiy : int, optional
            ROI size in the y-direction [pixels]
        acq_mode : str, optional
            Predefined acquisition mode (default=None)

        Example:
        --------
        m.acquire_darks(nimages_darks=100, exposure_time=5)
        """
 
        if nimages_dark != None:
            self.nimages_dark = nimages_dark
        if exposure_time != None:
            self.exposure_time = exposure_time
        if exposure_period != None:
            self.exposure_period = exposure_period
        if roix != None:
            self.roix = roix
        if roiy != None:
            self.roiy = roiy
        
        self.build_filename(acquisition_type='dark')

        ### TODO: camera reset
        print("Handing over to 'scans.acquire_dark")
        scans.acquire_dark(exp_burst=self.nimages_dark, exp_time=self.exposure_time, exp_period=self.exposure_period, image_width=self.roix, 
                           image_height=self.roiy, acq_mode=acq_mode, file_path=self.file_path, nr_writers=2, base_path=self.base_path,
                           file_prefix=self.file_prefix)
        
        
    def acquire_whites(self,nimages_white=None, sample_angle_out=None, sample_position_out=None,
                       exposure_time=None, exposure_period=None,
                       roix=None, roiy=None, acq_mode=None):
        """
        Acquire a set of whites images with shutters open and sample out of beam.

        Parameters
        ----------
        nimages_whites : int, optional
            Number of white images to acquire (no default)
        sample_angle_out : float, optional
            Sample rotation angle for sample out of the beam [deg]
        sample_position_out : float, optional
            Sample stage X position for sample out of the beam [um]
        exposure_time : float, optional
            Exposure time [ms]. If not specified, the currently configured value on the camera will be used
        exposure_period : float, optional
            Exposure period [ms]
        roix : int, optional
            ROI size in the x-direction [pixels]
        roiy : int, optional
            ROI size in the y-direction [pixels]
        acq_mode : str, optional
            Predefined acquisition mode (default=None)

        Example:
        --------
        m.acquire_whites(nimages_whites=100, exposure_time=5)
        """
        
        if nimages_white != None:
            self.nimages_white = nimages_white
        if sample_angle_out != None:
            self.sample_angle_out = sample_angle_out
        if sample_position_out != None:
            self.sample_position_out = sample_position_out
        if exposure_time != None:
            self.exposure_time = exposure_time
        if exposure_period != None:
            self.exposure_period = exposure_period
        if roix != None:
            self.roix = roix
        if roiy != None:
            self.roiy = roiy
        
        self.build_filename(acquisition_type='white')

        ### TODO: camera reset
        print("Handing over to 'scans.acquire_whites")
        scans.acquire_white(exp_burst=self.nimages_white, sample_angle_out=self.sample_angle_out, sample_position_out= self.sample_position_out,
                             exp_time=self.exposure_time, exp_period=self.exposure_period, image_width=self.roix,
                             image_height=self.roiy, acq_mode=acq_mode, file_path=self.file_path, nr_writers=2, base_path=self.base_path,
                             file_prefix=self.file_prefix)    
        

    def acquire_refs(self,nimages_dark=None, nimages_white=None, sample_angle_out=None,
                     sample_position_in=None, sample_position_out=None,
                     exposure_time=None, exposure_period=None,
                     roix=None, roiy=None, acq_mode=None):
        """
        Acquire reference images (darks + whites) and return to beam position.
        
        Reference images are acquired automatically in an optimized sequence and
        the sample is returned to the sample_in_position afterwards.

        Parameters
        ----------
        darks : int, optional
            Number of dark images to acquire (no default)
        nimages_whites : int, optional
            Number of white images to acquire (no default)
        sample_angle_out : float, optional
            Sample rotation angle for sample out of the beam [deg]
        sample_position_in : float, optional
            Sample stage X position for sample in of the beam [um]
        sample_position_out : float, optional
            Sample stage X position for sample out of the beam [um]
        exposure_time : float, optional
            Exposure time [ms]. If not specified, the currently configured value on the camera will be used
        exposure_period : float, optional
            Exposure period [ms]
        roix : int, optional
            ROI size in the x-direction [pixels]
        roiy : int, optional
            ROI size in the y-direction [pixels]
        acq_mode : str, optional
            Predefined acquisition mode (default=None)

        Example:
        --------
        m.acquire_refs(nimages_whites=100, exposure_time=5)
        """
        
        if nimages_dark != None:
            self.nimages_dark = nimages_dark
        if nimages_white != None:
            self.nimages_white = nimages_white
        if sample_angle_out != None:
            self.sample_angle_out = sample_angle_out
        if sample_position_in != None:
            self.sample_position_in = sample_position_in
        if sample_position_out != None:
            self.sample_position_out = sample_position_out
        if exposure_time != None:
            self.exposure_time = exposure_time
        if exposure_period != None:
            self.exposure_period = exposure_period
        if roix != None:
            self.roix = roix
        if roiy != None:
            self.roiy = roiy
        
        self.build_filename(acquisition_type='dark')
        file_prefix_dark = self.file_prefix
        self.build_filename(acquisition_type='white')
        file_prefix_white = self.file_prefix

        ### TODO: camera reset
        print("Handing over to 'scans.acquire_refs")
        scans.acquire_refs(num_darks=self.nimages_dark, num_flats=self.nimages_white, sample_angle_out=self.sample_angle_out,
                             sample_position_in=self.sample_position_in, sample_position_out=self.sample_position_out,
                             exp_time=self.exposure_time, exp_period=self.exposure_period, image_width=self.roix,
                             image_height=self.roiy, acq_mode='default', file_path=self.file_path, nr_writers=2, base_path=self.base_path,
                             file_prefix_dark=file_prefix_dark, file_prefix_white=file_prefix_white)        