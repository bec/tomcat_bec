from bec_widgets.cli.auto_updates import AutoUpdates, ScanInfo


class PlotUpdate(AutoUpdates):

    # def simple_line_scan(self, info: ScanInfo) -> None:
    #     """
    #     Simple line scan.
    #     """
    #     fig = self.get_default_figure()
    #     if not fig:
    #         return
    #     dev_x = info.scan_report_devices[0]
    #     dev_y = self.get_selected_device(info.monitored_devices, self.gui.selected_device)
    #     if not dev_y:
    #         return
    #     fig.clear_all()
    #     plt = fig.plot(x_name=dev_x, y_name=dev_y)
    #     plt.set(title=f"Custom Plot {info.scan_number}", x_label=dev_x, y_label=dev_y)

    def handler(self, info: ScanInfo) -> None:
        # EXAMPLES:
        # if info.scan_name == "line_scan" and info.scan_report_devices:
        #     self.simple_line_scan(info)
        #     return
        # if info.scan_name == "grid_scan" and info.scan_report_devices:
        #     self.run_grid_scan_update(info)
        #     return
        super().handler(info)
