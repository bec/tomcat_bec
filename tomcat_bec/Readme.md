# Instructions for using the BEC testbench at uXAS





## Setting up the environment

### Login

As the BEC requires modern libraries only available on RHEL8 and the X05LA beamline consoles are
ancient, the microXAS BEC installation runs on the *x05la-bec-001* virtual console. It is set up
as a special beamline console with the AWI software stack and is administered by AWI. All BEC 
services are set up such that the *gac-x05la* account can administer all services.
As any beamline console it's part of the *sls-bl-x05la* beamline network, so you have to go through
the SSH gateway by:
```
ssh -J x05la-gw gac-x05la@x05la-bec-001
```
PSI's virtual terminal service *nx-term.psi.ch* offers a convenient way to have a linux desktop
from your browser.

### Monitoring BEC services

Once logged in to the *x05la-bec-001* host, you can attach to the console of the BEC services by:
```
tmux attach -t bec
```
You can restart services by clicking on their sub-terminal and pressing Ctrl + C (sometimes twice).
This is needed whenever you edit the scan or device files.

### Activating the BEC environment

In order to start the BEC console interface, you first need to activate the corresponding venv.
This will also start the BEC GUI in a separate process and window:
```
source /data/test/x05la-test-bec/test/bec_venv/bin/activate
bec
```

### The BEC deployment

The BEC is deployed to a beamline specific network mounted folder that contains the main
repositories for the current installation, you can check it by:
```
(bec_venv) [gac-x05la@x05la-bec-001 ~]$ ls -ltr /data/test/x05la-test-bec/test/
total 3
drwxr-xr-x  7 gac-x05la unx-nogroup 4096 Nov 19 15:25 ophyd_devices
drwxr-xr-x  8 gac-x05la unx-nogroup 4096 Nov 19 15:25 bec_widgets
drwxr-xr-x  6 gac-x05la unx-nogroup 4096 Nov 19 15:26 bec_venv
-rw-r--r--  1 gac-x05la unx-nogroup  451 Nov 19 15:29 bec_config.yaml
drwxr-xr-x  6 gac-x05la unx-nogroup 4096 Nov 27 13:35 tomcat_bec
drwxr-xr-x 15 gac-x05la unx-nogroup 4096 Nov 27 14:03 bec
```
A short summary of the folder structure: 
  - bec_venv : The actual installation directory of the BEC framework
  - bec : Git repo for the BEC framework
  - bec_widgets : Git repo for the BEC widgets 
  - ophyd_devices : Git repo as installation directory for shared ophyd devices
  - tomcat_bec : Git repo as installation directory for Tomcat specific **scans, devices and configs.**

In short: You should activate the installation in the bec_venv directory and only edit the
tomcat_bec folder. And this is easily done in Visual Studio Code, a really handy IDE if you
have a fast connection:
```
code &
```

### The Tomcat BEC repository

The Tomcat BEC repository is a collection of Tomcat specific ophyd devices, scans, scripts and 
device configurations. It tries to enforce a standard structure across beamlines and it's
**maintenance is the responsibility of the beamline.** I.e. it should be handed over from
controls while still keeping the basic structure:
```
[gac-x05la@x05la-bec-001 ~]$ ls -ltr /data/test/x05la-test-bec/bec_deployment/tomcat_bec/tomcat_bec/
total 6
-rw-r--r-- 1 gac-x05la unx-sls        0 Jan  1  1970 __init__.py
drwxr-xr-x 2 gac-x05la unx-sls     4096 Aug 20 15:10 file_writer
drwxr-xr-x 2 gac-x05la unx-sls     4096 Aug 20 15:10 services
drwxr-xr-x 2 gac-x05la unx-sls     4096 Aug 20 15:15 __pycache__
drwxr-xr-x 4 gac-x05la unx-sls     4096 Aug 20 15:15 deployments
drwxr-xr-x 6 gac-x05la unx-sls     4096 Aug 21 11:11 bec_ipython_client
drwxr-xr-x 4 gac-x05la unx-sls     4096 Aug 21 17:52 bec_widgets
drwxr-xr-x 2 gac-x05la unx-sls     4096 Aug 29 09:43 logs
drwxr-xr-x 3 gac-x05la unx-sls     4096 Sep  3 13:28 device_configs
drwxr-xr-x 7 gac-x05la unx-sls     4096 Sep  5 18:08 devices
drwxr-xr-x 3 gac-x05la unx-sls     4096 Sep 16 12:30 scans
-rw-r--r-- 1 gac-x05la unx-nogroup 3383 Oct  1 16:08 Readme.md
```
From the list of folders the most important ones are:
  - device_configs : Ophyd device datbase as YAML file
  - devices : Ophyd device repository (i.e. Aerotech, Gigafrost)
  - scans : Tomcat specific base scans in the low-level API
  - scripts : Scripts and scans using the high level API

The official location of the beamline repository is:
```
https://gitlab.psi.ch/bec/tomcat_bec
```


### Configuration and basic diagnostic in BEC

Once you're at the CLI it is good practice to check the available devices:
```
dev.show_all()
```
If the previous two configuration attempts were unsuccesfull the bec_device_server will start
without any devices. A single missing or mistyped IOC will cause a configuration failure, so once
you fixed you configs you can reload your YAML config file:
```
bec.config.update_session_with_file("/data/test/x05la-test-bec/test/tomcat_bec/tomcat_bec/device_configs/microxas_test_bed.yaml")
```
If a single, unnecesary device doesn't come up , just comment it out.


## Early tests with GigaFrost and Aerotech

The main goal of the testbench is to test the various infrastructure components foreseen at the
Tomcat beamlines and ensure their collaboration. Namely the rotation stage with the Aerotech smart
controller, the GigaFrost camera and the Standard-DAQ recording system. The individual repositories
have more documentation on the components, but as a short summary, the Aerotech Automation1 IOC is
a recently developed EPICS driver, the GigaFrost IOC is an ancient offshoot of Helge's camera driver
and the stdDAQ is a non-EPICS device that is controlled via a hybrid websocket and REST interface. 
The DAQ also provides preview streams with hardcoded throttling. 

Note that while in the bluesky event model a manual configuration step sets up the device for a
scan. Followed by commands stage and trigger for stepping interface and kickoff and collect for
flyers. While in the BEC event model, stage is called on all enabled deviced on the deviceserver.
Calling stage also configures the device from the scaninfo. Moreover all steges are called in
parallel, regardless of device order. Redundant staging raises an exception.

### Seeing is believing (GUI)

Since seeing is believing, it's often a good idea to bring up some panels and get visual
feedback. You can start the standard CaQtDM panels for the Aerotech and GigaFrost from any
beamline console running the contros stack by:
```
caqtdm -macro "CAM=X02DA-CAM-GF2" X_X02DA_GIGAFROST_camControl_user.ui
caqtdm -macro "P=X02DA-ES1-SMP1" aeroauto1_ControllerOverview.ui
# PCO Edge camera panel
caqtdm -macro "NAME=X02DA-CCDCAM2" CameraExpert.ui
```

The std-DAQ doesn't have control panels, but the BEC can display the hard-coded preview streams.
The *daq_stream0* preview forwards displays 1 frames at a maximum of 5 Hz.
So we just need to add the correct widgets to the GUI:
```
gui.add_dock('tomolive')
gui.panels['tomolive'].add_widget('BECFigure').image('daq_stream0')
```


## BEC scans at the testbench

The concept of scans is slightly different in bluesky and BEC. Bluesky strictly separates
the device logic in ophyd from the scan logic in plans. Devices are passed in as arguments
i.e. their behavior is fully managed by the called scan plan in a single layer. 

On the other hand, BEC has two scan interfaces and additional device specific scan logic is
placed on the ophyd device itself (total of three levels). Theese are:
  - Device self-configuration from the scaninfo on the ophyd device
  - Low-level scan API on the ScanServer
  - High-level scripting in the BEC console

The general idea is that the lower-level scan API should be generic with minimal device specific
logic (although some examples counter this argument), so the same scan can be re-used across
multiple devices. Device specific scan behavior and configuration is defined either on the ophyd
device or in the high level interface. In practice, the former means that a device must fish for
relevant parameters in the 'scaninfo' message, that's passed around the beginning of each scan.

Another major difference is that **ALL devices** live on the DeviceServer, and automatically
participate in scans whenever they are enabled. This adds the role of scope management to the
scripting interface, i.e. enabling/disabling of devices for the particular scan.

The current repository has several implemented scans to test compliance with standard BEC
interfaces and partly cover the old EPICS IOC functionality.

  - ALL enabled devices will participate in the scan
	- If a device is enabled for a scan it will be staged (i.e. launch acquisition, script, etc...)
	- Yes, you have to actively manage what's enabled and what's not
	- Even a motor+diode scan must disable not relevant devices across the beamline
  - Latest by the time stage finishes it must be configured
    - Either manually by the preparation or scan routine
    - Or from fishing it out from the scan parameters (generally by looking for specific kwargs)
	- Some fished scan parameters might overwrite prior configuration
  - The device will not try to make sure that the received configuration is correct
    - So tough luck if you're reusing the same parameters across different scans

  
  


### anotherstepscan

Simple software step scan in the low-level API that almost calls the standard BEC scan routine.
Its only difference is that it overrides the per-point behavior to use Gigafrost's HW burst mode
instead of sending multiple software triggers. The ophyd devices are capable to allocate the
required number of points for DAQ and position readback.

```
demosanotherstepscantepscan(scan_start, scan_end, steps, exp_time=0.005, burst_at_each_point=1, settling_time=0, sync='event')
```

### snapnstep

AeroScript controlled high-speed step-scan that sets up the devices and calls the corresponding
low-level scan API to substitute the jinja2 template file and launch the script. The substitution
logic was placed in the low-level API, as it's very similar to other AeroScript scans.
The actual scan logic is in the **AerotechSnapAndStepTemplate.ascript** file. PSO and data 
readback are configured and launched internally. It also waits for the script to finish.
```
anothersnapnstepscan(scan_start, scan_end, 180, exp_time=0.005, exp_burst=1800, repeats=10)
```


### anothersequencescan

AeroScript controlled fly scan that sets up the devices and calls the corresponding low-level scan
to substitute the jinja2 template file and launch the script. The actual scan logic is in the 
**AerotechSimpleSequenceTemplate.ascript** file. PSO and data readback are configured
and launched internally. It also waits for the script to finish. It might have problems with
configuring the readback of the number of repeats because that's done on the IOC,
```
anothersequencescan(scan_start, 180, 180, exp_time=0.005, exp_burst=1800, repeats=10)
```



