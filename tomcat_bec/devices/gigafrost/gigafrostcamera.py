# -*- coding: utf-8 -*-
"""
GigaFrost camera class module

Created on Thu Jun 27 17:28:43 2024

@author: mohacsi_i
"""
from time import sleep
from ophyd import Signal, SignalRO, Component, EpicsSignal, EpicsSignalRO, Kind, DeviceStatus
from ophyd.device import DynamicDeviceComponent
from ophyd_devices.interfaces.base_classes.psi_detector_base import (
    CustomDetectorMixin,
    PSIDetectorBase,
)

try:
    import gfconstants as const
except ModuleNotFoundError:
    import tomcat_bec.devices.gigafrost.gfconstants as const

try:
    from gfutils import extend_header_table
except ModuleNotFoundError:
    from tomcat_bec.devices.gigafrost.gfutils import extend_header_table

try:
    from bec_lib import bec_logger

    logger = bec_logger.logger
except ModuleNotFoundError:
    import logging

    logger = logging.getLogger("GfCam")


class GigaFrostCameraMixin(CustomDetectorMixin):
    """Mixin class to setup TOMCAT specific implementations of the detector.

    This class will be called by the custom_prepare_cls attribute of the
    detector class.
    """
    # pylint: disable=protected-access
    def _define_backend_ip(self):
        """Select backend IP address for UDP stream"""
        if self.parent.backendUrl.get() == const.BE3_DAFL_CLIENT:  # xbl-daq-33
            return const.BE3_NORTH_IP, const.BE3_SOUTH_IP
        if self.parent.backendUrl.get() == const.BE999_DAFL_CLIENT:
            return const.BE999_NORTH_IP, const.BE999_SOUTH_IP

        raise RuntimeError(f"Backend {self.parent.backendUrl.get()} not recognized.")

    def _define_backend_mac(self):
        """Select backend MAC address for UDP stream"""
        if self.parent.backendUrl.get() == const.BE3_DAFL_CLIENT:  # xbl-daq-33
            return const.BE3_NORTH_MAC, const.BE3_SOUTH_MAC
        if self.parent.backendUrl.get() == const.BE999_DAFL_CLIENT:
            return const.BE999_NORTH_MAC, const.BE999_SOUTH_MAC

        raise RuntimeError(f"Backend {self.parent.backendUrl.get()} not recognized.")

    def _set_udp_header_table(self):
        """Set the communication parameters for the camera module"""
        self.parent.cfgConnectionParam.set(self._build_udp_header_table()).wait()

    def _build_udp_header_table(self):
        """Build the header table for the UDP communication"""
        udp_header_table = []

        for i in range(0, 64, 1):
            for j in range(0, 8, 1):
                dest_port = 2000 + 8 * i + j
                source_port = 3000 + j
                if j < 4:
                    extend_header_table(
                        udp_header_table,
                        self.parent.macSouth.get(),
                        self.parent.ipSouth.get(),
                        dest_port,
                        source_port,
                    )
                else:
                    extend_header_table(
                        udp_header_table,
                        self.parent.macNorth.get(),
                        self.parent.ipNorth.get(),
                        dest_port,
                        source_port,
                    )

        return udp_header_table

    def on_init(self) -> None:
        """Initialize the camera, set channel values"""
        # ToDo: Not sure if it's a good idea to change camera settings upon
        # ophyd device startup, i.e. each deviceserver restart.
        self._init_gigafrost()
        self.parent._initialized = True

    def _init_gigafrost(self) -> None:
        """Initialize the camera, set channel values"""
        # Stop acquisition
        self.parent.cmdStartCamera.set(0).wait()

        # set entry to UDP table
        # number of UDP ports to use
        self.parent.cfgUdpNumPorts.set(2).wait()
        # number of images to send to each UDP port before switching to next
        self.parent.cfgUdpNumFrames.set(5).wait()
        # offset in UDP table - where to find the first entry
        self.parent.cfgUdpHtOffset.set(0).wait()
        # activate changes
        self.parent.cmdWriteService.set(1).wait()

        # Configure software triggering if needed
        if self.parent.autoSoftEnable.get():
            # trigger modes
            self.parent.cfgCntStartBit.set(1).wait()
            self.parent.cfgCntEndBit.set(0).wait()

            # set modes
            self.parent.enable_mode = "soft"
            self.parent.trigger_mode = "auto"
            self.parent.exposure_mode = "timer"

        # line swap - on for west, off for east
        self.parent.cfgLineSwapSW.set(1).wait()
        self.parent.cfgLineSwapNW.set(1).wait()
        self.parent.cfgLineSwapSE.set(0).wait()
        self.parent.cfgLineSwapNE.set(0).wait()

        # Commit parameters
        self.parent.cmdSetParam.set(1).wait()

        # Initialize data backend
        n, s = self._define_backend_ip()
        self.parent.ipNorth.put(n, force=True)
        self.parent.ipSouth.put(s, force=True)
        n, s = self._define_backend_mac()
        self.parent.macNorth.put(n, force=True)
        self.parent.macSouth.put(s, force=True)
        # Set udp header table
        self._set_udp_header_table()

        return super().on_init()

    def on_stage(self) -> None:
        """Configuration and staging

        In the BEC model ophyd devices must fish out their own configuration from the 'scaninfo'.
        I.e. they need to know which parameters are relevant for them at each scan.

        NOTE: Tomcat might use multiple cameras.
        """
        # Gigafrost can finish a run without explicit unstaging
        if self.parent.infoBusyFlag.value:
            logger.warning("Camera is already running, unstaging it first!")
            self.parent.unstage()
            sleep(0.5)
        if not self.parent._initialized:
            logger.warning(
                f"[{self.parent.name}] Ophyd device havent ran the initialization sequence,"
                "IOC might be in unknown configuration."
            )

        # Fish out our configuration from scaninfo (via explicit or generic addressing)
        scanparam = self.parent.scaninfo.scan_msg.info
        d = {}
        if "kwargs" in scanparam:
            scanargs = scanparam["kwargs"]
            if "image_width" in scanargs and scanargs["image_width"] is not None:
                d["image_width"] = scanargs["image_width"]
            if "image_height" in scanargs and scanargs["image_height"] is not None:
                d["image_height"] = scanargs["image_height"]
            if "exp_time" in scanargs and scanargs["exp_time"] is not None:
                d["exposure_time_ms"] = scanargs["exp_time"]
            if "exp_burst" in scanargs and scanargs["exp_burst"] is not None:
                d["exposure_num_burst"] = scanargs["exp_burst"]
            if "acq_mode" in scanargs and scanargs["acq_mode"] is not None:
                d["acq_mode"] = scanargs["acq_mode"]
            # elif self.parent.scaninfo.scan_type == "step":
            #     d['acq_mode'] = "default"

        # Perform bluesky-style configuration
        if len(d) > 0:
            logger.warning(f"[{self.parent.name}] Configuring with:\n{d}")
            self.parent.configure(d=d)

        # Sync if out of sync
        if self.parent.infoSyncFlag.value == 0:
            self.parent.cmdSyncHw.set(1).wait()
        # Switch to acquiring
        self.parent.bluestage()

    def on_unstage(self) -> None:
        """Specify actions to be executed during unstage.

        This step should include checking if the acquisition was successful,
        and publishing the file location and file event message,
        with flagged done to BEC.
        """
        # Switch to idle
        self.parent.cmdStartCamera.set(0).wait()
        if self.parent.autoSoftEnable.get():
            self.parent.cmdSoftEnable.set(0).wait()

    def on_stop(self) -> None:
        """
        Specify actions to be executed during stop.
        This must also set self.parent.stopped to True.

        This step should include stopping the detector and backend service.
        """
        return self.on_unstage()

    def on_trigger(self) -> None | DeviceStatus:
        """
        Specify actions to be executed upon receiving trigger signal.
        Return a DeviceStatus object or None
        """
        if self.parent.infoBusyFlag.get() in (0, "IDLE"):
            raise RuntimeError("GigaFrost must be running before triggering")

        logger.warning(f"[{self.parent.name}] SW triggering gigafrost")

        # Soft triggering based on operation mode
        if (
            self.parent.autoSoftEnable.get()
            and self.parent.trigger_mode == "auto"
            and self.parent.enable_mode == "soft"
        ):
            # BEC teststand operation mode: posedge of SoftEnable if Started
            self.parent.cmdSoftEnable.set(0).wait()
            self.parent.cmdSoftEnable.set(1).wait()
        else:
            self.parent.cmdSoftTrigger.set(1).wait()


class GigaFrostCamera(PSIDetectorBase):
    """Ophyd device class to control Gigafrost cameras at Tomcat

    The actual hardware is implemented by an IOC based on an old fork of Helge's
    cameras. This means that the camera behaves differently than the SF cameras
    in particular it provides even less feedback about it's internal progress.
    Helge will update the GigaFrost IOC after working beamline.
    The ophyd class is based on the 'gfclient' package and has a lot of Tomcat
    specific additions. It does behave differently though, as ophyd swallows the
    errors from failed PV writes.

    Parameters
    ----------
    use_soft_enable : bool
        Flag to use the camera's soft enable (default: False)
    backend_url : str
        Backend url address necessary to set up the camera's udp header.
        (default: http://xbl-daq-23:8080)

    Bugs:
    ----------
    FRAMERATE : Ignored in soft trigger mode, period becomes 2xExposure time
    """

    # pylint: disable=too-many-instance-attributes

    custom_prepare_cls = GigaFrostCameraMixin
    USER_ACCESS = ["initialize"]
    _initialized = False

    infoBusyFlag = Component(EpicsSignalRO, "BUSY_STAT", auto_monitor=True)
    infoSyncFlag = Component(EpicsSignalRO, "SYNC_FLAG", auto_monitor=True)
    cmdSyncHw = Component(EpicsSignal, "SYNC_SWHW.PROC", put_complete=True, kind=Kind.omitted)
    cmdStartCamera = Component(EpicsSignal, "START_CAM", put_complete=True, kind=Kind.omitted)
    cmdSetParam = Component(EpicsSignal, "SET_PARAM.PROC", put_complete=True, kind=Kind.omitted)
    cfgAcqMode = Component(EpicsSignal, "ACQMODE", put_complete=True, kind=Kind.config)

    array_size = DynamicDeviceComponent(
        {
            "array_size_x": (EpicsSignalRO, "ROIX", {"auto_monitor": True}),
            "array_size_y": (EpicsSignalRO, "ROIY", {"auto_monitor": True}),
        },
        doc="Size of the array in the XY dimensions",
    )

    # UDP header
    cfgUdpNumPorts = Component(EpicsSignal, "PORTS", put_complete=True, kind=Kind.config)
    cfgUdpNumFrames = Component(EpicsSignal, "FRAMENUM", put_complete=True, kind=Kind.config)
    cfgUdpHtOffset = Component(EpicsSignal, "HT_OFFSET", put_complete=True, kind=Kind.config)
    cmdWriteService = Component(EpicsSignal, "WRITE_SRV.PROC", put_complete=True, kind=Kind.omitted)

    # Standard camera configs
    cfgExposure = Component(
        EpicsSignal, "EXPOSURE", put_complete=True, auto_monitor=True, kind=Kind.config
    )
    cfgFramerate = Component(
        EpicsSignal, "FRAMERATE", put_complete=True, auto_monitor=True, kind=Kind.config
    )
    cfgRoiX = Component(EpicsSignal, "ROIX", put_complete=True, auto_monitor=True, kind=Kind.config)
    cfgRoiY = Component(EpicsSignal, "ROIY", put_complete=True, auto_monitor=True, kind=Kind.config)
    cfgScanId = Component(
        EpicsSignal, "SCAN_ID", put_complete=True, auto_monitor=True, kind=Kind.config
    )
    cfgCntNum = Component(
        EpicsSignal, "CNT_NUM", put_complete=True, auto_monitor=True, kind=Kind.config
    )
    cfgCorrMode = Component(
        EpicsSignal, "CORR_MODE", put_complete=True, auto_monitor=True, kind=Kind.config
    )

    # Software signals
    cmdSoftEnable = Component(EpicsSignal, "SOFT_ENABLE", put_complete=True)
    cmdSoftTrigger = Component(EpicsSignal, "SOFT_TRIG.PROC", put_complete=True, kind=Kind.omitted)
    cmdSoftExposure = Component(EpicsSignal, "SOFT_EXP", put_complete=True)
    cfgAcqMode = Component(EpicsSignal, "ACQMODE", put_complete=True, kind=Kind.config)

    ###############################################################################################
    # Enable schemes
    # NOTE: 0 physical, 1 virtual (i.e. always running, but logs enable signal)
    cfgEnableScheme = Component(
        EpicsSignal,
        "MODE_ENBL_EXP_RBV",
        write_pv="MODE_ENBL_EXP",
        put_complete=True,
        kind=Kind.config,
    )
    # Enable signals (combined by OR gate)
    cfgEnableExt = Component(
        EpicsSignal,
        "MODE_ENBL_EXT_RBV",
        write_pv="MODE_ENBL_EXT",
        put_complete=True,
        kind=Kind.config,
    )
    cfgEnableSoft = Component(
        EpicsSignal,
        "MODE_ENBL_SOFT_RBV",
        write_pv="MODE_ENBL_SOFT",
        put_complete=True,
        kind=Kind.config,
    )
    cfgEnableAlways = Component(
        EpicsSignal,
        "MODE_ENBL_AUTO_RBV",
        write_pv="MODE_ENBL_AUTO",
        put_complete=True,
        kind=Kind.config,
    )

    ###############################################################################################
    # Trigger modes
    cfgTrigExt = Component(
        EpicsSignal,
        "MODE_TRIG_EXT_RBV",
        write_pv="MODE_TRIG_EXT",
        put_complete=True,
        kind=Kind.config,
    )
    cfgTrigSoft = Component(
        EpicsSignal,
        "MODE_TRIG_SOFT_RBV",
        write_pv="MODE_TRIG_SOFT",
        put_complete=True,
        kind=Kind.config,
    )
    cfgTrigTimer = Component(
        EpicsSignal,
        "MODE_TRIG_TIMER_RBV",
        write_pv="MODE_TRIG_TIMER",
        put_complete=True,
        kind=Kind.config,
    )
    cfgTrigAuto = Component(
        EpicsSignal,
        "MODE_TRIG_AUTO_RBV",
        write_pv="MODE_TRIG_AUTO",
        put_complete=True,
        kind=Kind.config,
    )

    ###############################################################################################
    # Exposure modes
    # NOTE: I.e.exposure time control, usually TIMER
    cfgExpExt = Component(
        EpicsSignal,
        "MODE_EXP_EXT_RBV",
        write_pv="MODE_EXP_EXT",
        put_complete=True,
        kind=Kind.config,
    )
    cfgExpSoft = Component(
        EpicsSignal,
        "MODE_EXP_SOFT_RBV",
        write_pv="MODE_EXP_SOFT",
        put_complete=True,
        kind=Kind.config,
    )
    cfgExpTimer = Component(
        EpicsSignal,
        "MODE_EXP_TIMER_RBV",
        write_pv="MODE_EXP_TIMER",
        put_complete=True,
        kind=Kind.config,
    )

    ###############################################################################################
    # Trigger configuration PVs
    # NOTE: Theese PVs set the behavior on posedge and negedge of the trigger signal
    cfgCntStartBit = Component(
        EpicsSignal,
        "CNT_STARTBIT_RBV",
        write_pv="CNT_STARTBIT",
        put_complete=True,
        kind=Kind.config,
    )
    cfgCntEndBit = Component(
        EpicsSignal, "CNT_ENDBIT_RBV", write_pv="CNT_ENDBIT", put_complete=True, kind=Kind.config
    )

    # Line swap selection
    cfgLineSwapSW = Component(EpicsSignal, "LS_SW", put_complete=True, kind=Kind.config)
    cfgLineSwapNW = Component(EpicsSignal, "LS_NW", put_complete=True, kind=Kind.config)
    cfgLineSwapSE = Component(EpicsSignal, "LS_SE", put_complete=True, kind=Kind.config)
    cfgLineSwapNE = Component(EpicsSignal, "LS_NE", put_complete=True, kind=Kind.config)
    cfgConnectionParam = Component(
        EpicsSignal, "CONN_PARM", string=True, put_complete=True, kind=Kind.config
    )

    # HW settings as read only
    cfgSyncFlag = Component(EpicsSignalRO, "PIXRATE", auto_monitor=True, kind=Kind.config)
    cfgTrigDelay = Component(EpicsSignalRO, "TRIG_DELAY", auto_monitor=True, kind=Kind.config)
    cfgSyncoutDelay = Component(EpicsSignalRO, "SYNCOUT_DLY", auto_monitor=True, kind=Kind.config)
    cfgOutputPolarity0 = Component(EpicsSignalRO, "BNC0_RBV", auto_monitor=True, kind=Kind.config)
    cfgOutputPolarity1 = Component(EpicsSignalRO, "BNC1_RBV", auto_monitor=True, kind=Kind.config)
    cfgOutputPolarity2 = Component(EpicsSignalRO, "BNC2_RBV", auto_monitor=True, kind=Kind.config)
    cfgOutputPolarity3 = Component(EpicsSignalRO, "BNC3_RBV", auto_monitor=True, kind=Kind.config)
    cfgInputPolarity1 = Component(EpicsSignalRO, "BNC4_RBV", auto_monitor=True, kind=Kind.config)
    cfgInputPolarity2 = Component(EpicsSignalRO, "BNC5_RBV", auto_monitor=True, kind=Kind.config)
    infoBoardTemp = Component(EpicsSignalRO, "T_BOARD", auto_monitor=True)

    USER_ACCESS = ["exposure_mode", "fix_nframes_mode", "trigger_mode", "enable_mode", "initialize"]

    autoSoftEnable = Component(Signal, kind=Kind.config)
    backendUrl = Component(Signal, kind=Kind.config)
    macNorth = Component(Signal, kind=Kind.config)
    macSouth = Component(Signal, kind=Kind.config)
    ipNorth = Component(Signal, kind=Kind.config)
    ipSouth = Component(Signal, kind=Kind.config)

    def __init__(
        self,
        prefix="",
        *,
        name,
        kind=None,
        read_attrs=None,
        configuration_attrs=None,
        parent=None,
        device_manager=None,
        sim_mode=False,
        auto_soft_enable=False,
        backend_url=const.BE999_DAFL_CLIENT,
        **kwargs,
    ):
        # Ugly hack to pass values before on_init()
        self._signals_to_be_set = {}
        self._signals_to_be_set["auto_soft_enable"] = auto_soft_enable
        self._signals_to_be_set["backend_url"] = backend_url

        # super() will call the mixin class
        super().__init__(
            prefix=prefix,
            name=name,
            kind=kind,
            read_attrs=read_attrs,
            configuration_attrs=configuration_attrs,
            parent=parent,
            device_manager=device_manager,
            **kwargs,
        )

    def _init(self):
        """Ugly hack: values must be set before on_init() is called"""
        # Additional parameters
        self.autoSoftEnable._metadata["write_access"] = False
        self.backendUrl._metadata["write_access"] = False
        self.autoSoftEnable.put(self._signals_to_be_set["auto_soft_enable"], force=True)
        self.backendUrl.put(self._signals_to_be_set["backend_url"], force=True)
        return super()._init()

    def initialize(self):
        """Initialization in separate command"""
        self.custom_prepare._init_gigafrost()
        self._initialized = True

    def trigger(self) -> DeviceStatus:
        """Sends a software trigger to GigaFrost"""
        super().trigger()

        # There's no status readback from the camera, so we just wait
        sleep_time = self.cfgExposure.value * self.cfgCntNum.value * 0.001 + 0.2
        sleep(sleep_time)
        return DeviceStatus(self, done=True, success=True, settle_time=sleep_time)

    def configure(self, d: dict = None):
        """Configure the next scan with the GigaFRoST camera

        Parameters as 'd' dictionary
        ----------------------------
        num_images : int, optional
            Number of images to be taken during each scan. Set to -1 for an
            unlimited number of images (limited by the ringbuffer size and
            backend speed). (default = 10)
        exposure_time_ms : float, optional
            Exposure time [ms]. (default = 0.2)
        exposure_period_ms : float, optional
            Exposure period [ms], ignored in soft trigger mode. (default = 1.0)
        image_width : int, optional
            ROI size in the x-direction [pixels] (default = 2016)
        image_height : int, optional
            ROI size in the y-direction [pixels] (default = 2016)
        scanid : int, optional
            Scan identification number to be associated with the scan data
            (default = 0)
        correction_mode : int, optional
            The correction to be applied to the imaging data. The following
            modes are available (default = 5):

            * 0: Bypass. No corrections are applied to the data.
            * 1: Send correction factor A instead of pixel values
            * 2: Send correction factor B instead of pixel values
            * 3: Send correction factor C instead of pixel values
            * 4: Invert pixel values, but do not apply any linearity correction
            * 5: Apply the full linearity correction
        acq_mode : str, optional
            Select one of the pre-configured trigger behavior
        """
        # Stop acquisition
        self.unstage()
        if not self._initialized:
            pass

        # If Bluesky style configure
        if d is not None:
            # Commonly changed settings
            if "exposure_num_burst" in d:
                self.cfgCntNum.set(d["exposure_num_burst"]).wait()
            if "exposure_time_ms" in d:
                self.cfgExposure.set(d["exposure_time_ms"]).wait()
            if "exposure_period_ms" in d:
                self.cfgFramerate.set(d["exposure_period_ms"]).wait()
            if "image_width" in d:
                if d["image_width"] % 48 != 0:
                    raise RuntimeError(f"[{self.name}] image_width must be divisible by 48")
                self.cfgRoiX.set(d["image_width"]).wait()
            if "image_height" in d:
                if d["image_height"] % 16 != 0:
                    raise RuntimeError(f"[{self.name}] image_height must be divisible by 16")
                self.cfgRoiY.set(d["image_height"]).wait()
            # Dont change these
            scanid = d.get("scanid", 0)
            correction_mode = d.get("correction_mode", 5)
            self.cfgScanId.set(scanid).wait()
            self.cfgCorrMode.set(correction_mode).wait()

            if "acq_mode" in d:
                self.set_acquisition_mode(d["acq_mode"])

        # Commit parameters
        self.cmdSetParam.set(1).wait()

    def bluestage(self):
        """Bluesky style stage"""
        # Switch to acquiring
        self.cmdStartCamera.set(1).wait()

    def set_acquisition_mode(self, acq_mode):
        """Set acquisition mode

        Utility function to quickly select between pre-configured and tested
        acquisition modes.

        NOTE: The trigger input appears to be dead, it completely ignores the
        supplied signal. Use external enable instead, that works!
        """

        if acq_mode == "default":
            # Trigger parameters
            self.cfgCntStartBit.set(1).wait()
            self.cfgCntEndBit.set(0).wait()

            # Switch to physical enable signal
            self.cfgEnableScheme.set(0).wait()

            # Set modes
            # self.cmdSoftEnable.set(0).wait()
            self.enable_mode = "soft"
            self.trigger_mode = "auto"
            self.exposure_mode = "timer"
        elif acq_mode in ["ext_enable", "external_enable"]:
            # Switch to physical enable signal
            self.cfgEnableScheme.set(0).wait()
            # Trigger modes
            self.cfgCntStartBit.set(1).wait()
            self.cfgCntEndBit.set(0).wait()
            # Set modes
            self.enable_mode = "external"
            self.trigger_mode = "auto"
            self.exposure_mode = "timer"
        elif acq_mode == "soft":
            # Switch to physical enable signal
            self.cfgEnableScheme.set(0).wait()
            # Set enable signal to always
            self.cfgEnableExt.set(0).wait()
            self.cfgEnableSoft.set(1).wait()
            self.cfgEnableAlways.set(1).wait()
            # Set trigger mode to software
            self.cfgTrigExt.set(0).wait()
            self.cfgTrigSoft.set(1).wait()
            self.cfgTrigTimer.set(1).wait()
            self.cfgTrigAuto.set(0).wait()
            # Set exposure mode to timer
            self.cfgExpExt.set(0).wait()
            self.cfgExpSoft.set(0).wait()
            self.cfgExpTimer.set(1).wait()
            # Set trigger edge to fixed frames on posedge
            self.cfgCntStartBit.set(1).wait()
            self.cfgCntEndBit.set(0).wait()
        elif acq_mode in ["ext", "external"]:
            # Switch to physical enable signal
            self.cfgEnableScheme.set(0).wait()
            # Set enable signal to always
            self.cfgEnableExt.set(0).wait()
            self.cfgEnableSoft.set(0).wait()
            self.cfgEnableAlways.set(1).wait()
            # Set trigger mode to external
            self.cfgTrigExt.set(1).wait()
            self.cfgTrigSoft.set(0).wait()
            self.cfgTrigTimer.set(0).wait()
            self.cfgTrigAuto.set(0).wait()
            # Set exposure mode to timer
            self.cfgExpExt.set(0).wait()
            self.cfgExpSoft.set(0).wait()
            self.cfgExpTimer.set(1).wait()
            # Set trigger edge to fixed frames on posedge
            self.cfgCntStartBit.set(1).wait()
            self.cfgCntEndBit.set(0).wait()
        else:
            raise RuntimeError(f"Unsupported acquisition mode: {acq_mode}")

    @property
    def exposure_mode(self):
        """Returns the current exposure mode of the GigaFRost camera.

        Returns
        -------
        exp_mode : {'external', 'timer', 'soft'}
            The camera's active exposure mode.
            If more than one mode is active at the same time, it returns None.
        """
        mode_soft = self.cfgExpSoft.get()
        mode_timer = self.cfgExpTimer.get()
        mode_external = self.cfgExpExt.get()
        if mode_soft and not mode_timer and not mode_external:
            return "soft"
        if not mode_soft and mode_timer and not mode_external:
            return "timer"
        if not mode_soft and not mode_timer and mode_external:
            return "external"

        return None

    @exposure_mode.setter
    def exposure_mode(self, exp_mode):
        """Apply the exposure mode for the GigaFRoST camera.

        Parameters
        ----------
        exp_mode : {'external', 'timer', 'soft'}
            The exposure mode to be set.
        """
        if exp_mode == "external":
            self.cfgExpExt.set(1).wait()
            self.cfgExpSoft.set(0).wait()
            self.cfgExpTimer.set(0).wait()
        elif exp_mode == "timer":
            self.cfgExpExt.set(0).wait()
            self.cfgExpSoft.set(0).wait()
            self.cfgExpTimer.set(1).wait()
        elif exp_mode == "soft":
            self.cfgExpExt.set(0).wait()
            self.cfgExpSoft.set(1).wait()
            self.cfgExpTimer.set(0).wait()
        else:
            raise ValueError(
                f"Invalid exposure mode! Valid modes are:\n{const.gf_valid_exposure_modes}"
            )

        # Commit parameters
        self.cmdSetParam.set(1).wait()

    @property
    def fix_nframes_mode(self):
        """Return the current fixed number of frames mode of the GigaFRoST camera.

        Returns
        -------
        fix_nframes_mode : {'off', 'start', 'end', 'start+end'}
            The camera's active fixed number of frames mode.
        """
        start_bit = self.cfgCntStartBit.get()
        end_bit = self.cfgCntStartBit.get()

        if not start_bit and not end_bit:
            return "off"
        if start_bit and not end_bit:
            return "start"
        if not start_bit and end_bit:
            return "end"
        if start_bit and end_bit:
            return "start+end"

        return None

    @fix_nframes_mode.setter
    def fix_nframes_mode(self, mode):
        """Apply the fixed number of frames settings to the GigaFRoST camera.

        Parameters
        ----------
        mode : {'off', 'start', 'end', 'start+end'}
            The fixed number of frames mode to be applied.
        """
        self._fix_nframes_mode = mode
        if self._fix_nframes_mode == "off":
            self.cfgCntStartBit.set(0).wait()
            self.cfgCntEndBit.set(0).wait()
        elif self._fix_nframes_mode == "start":
            self.cfgCntStartBit.set(1).wait()
            self.cfgCntEndBit.set(0).wait()
        elif self._fix_nframes_mode == "end":
            self.cfgCntStartBit.set(0).wait()
            self.cfgCntEndBit.set(1).wait()
        elif self._fix_nframes_mode == "start+end":
            self.cfgCntStartBit.set(1).wait()
            self.cfgCntEndBit.set(1).wait()
        else:
            raise ValueError(
                f"Invalid fixed frame number mode! Valid modes are: {const.gf_valid_fix_nframe_modes}"
            )

        # Commit parameters
        self.cmdSetParam.set(1).wait()

    @property
    def trigger_mode(self):
        """Method to detect the current trigger mode set in the GigaFRost camera.

        Returns
        -------
        mode : {'auto', 'external', 'timer', 'soft'}
            The camera's active trigger mode. If more than one mode is active
            at the moment, None is returned.
        """
        mode_auto = self.cfgTrigAuto.get()
        mode_external = self.cfgTrigExt.get()
        mode_timer = self.cfgTrigTimer.get()
        mode_soft = self.cfgTrigSoft.get()
        if mode_auto:
            return "auto"
        if mode_soft:
            return "soft"
        if mode_timer:
            return "timer"
        if mode_external:
            return "external"

        return None

    @trigger_mode.setter
    def trigger_mode(self, mode):
        """Set the trigger mode for the GigaFRoST camera.

        Parameters
        ----------
        mode : {'auto', 'external', 'timer', 'soft'}
            The GigaFRoST trigger mode.
        """
        if mode == "auto":
            self.cfgTrigAuto.set(1).wait()
            self.cfgTrigSoft.set(0).wait()
            self.cfgTrigTimer.set(0).wait()
            self.cfgTrigExt.set(0).wait()
        elif mode == "external":
            self.cfgTrigAuto.set(0).wait()
            self.cfgTrigSoft.set(0).wait()
            self.cfgTrigTimer.set(0).wait()
            self.cfgTrigExt.set(1).wait()
        elif mode == "timer":
            self.cfgTrigAuto.set(0).wait()
            self.cfgTrigSoft.set(0).wait()
            self.cfgTrigTimer.set(1).wait()
            self.cfgTrigExt.set(0).wait()
        elif mode == "soft":
            self.cfgTrigAuto.set(0).wait()
            self.cfgTrigSoft.set(1).wait()
            self.cfgTrigTimer.set(0).wait()
            self.cfgTrigExt.set(0).wait()
        else:
            raise ValueError(
                "Invalid trigger mode! Valid modes are:\n{const.gf_valid_trigger_modes}"
            )

        # Commit parameters
        self.cmdSetParam.set(1).wait()

    @property
    def enable_mode(self):
        """Return the enable mode set in the GigaFRoST camera.

        Returns
        -------
        enable_mode: {'soft', 'external', 'soft+ext', 'always'}
            The camera's active enable mode.
        """
        mode_soft = self.cfgEnableSoft.get()
        mode_external = self.cfgEnableExt.get()
        mode_always = self.cfgEnableAlways.get()
        if mode_always:
            return "always"
        elif mode_soft and mode_external:
            return "soft+ext"
        elif mode_soft and not mode_external:
            return "soft"
        elif mode_external and not mode_soft:
            return "external"
        else:
            return None

    @enable_mode.setter
    def enable_mode(self, mode):
        """
        Set the enable mode for the GigaFRoST camera.

        NOTE: Always does not seem to work and Enablesoft works like  a trigger

        Parameters
        ----------
        mode : {'soft', 'external', 'soft+ext', 'always'}
            The GigaFRoST enable mode. Valid arguments are:

            * 'soft':
                The GigaFRoST enable signal is supplied through a software
                signal
            * 'external':
                The GigaFRoST enable signal is supplied through an external TTL
                gating signal from the rotaiton stage or some other control
                unit
            * 'soft+ext':
                The GigaFRoST enable signal can be supplied either via the
                software signal or externally. The two signals are combined
                with a logical OR gate.
            * 'always':
                The GigaFRoST is always enabled.
                CAUTION: This mode is not compatible with the fixed number of
                frames modes!
        """

        if mode not in const.gf_valid_enable_modes:
            raise ValueError("Invalid enable mode! Valid modes are:\n{const.gf_valid_enable_modes}")

        if mode == "soft":
            self.cfgEnableExt.set(0).wait()
            self.cfgEnableSoft.set(1).wait()
            self.cfgEnableAlways.set(0).wait()
        elif mode == "external":
            self.cfgEnableExt.set(1).wait()
            self.cfgEnableSoft.set(0).wait()
            self.cfgEnableAlways.set(0).wait()
        elif mode == "soft+ext":
            self.cfgEnableExt.set(1).wait()
            self.cfgEnableSoft.set(1).wait()
            self.cfgEnableAlways.set(0).wait()
        elif mode == "always":
            self.cfgEnableExt.set(0).wait()
            self.cfgEnableSoft.set(0).wait()
            self.cfgEnableAlways.set(1).wait()
        # Commit parameters
        self.cmdSetParam.set(1).wait()


# Automatically connect to MicroSAXS testbench if directly invoked
if __name__ == "__main__":
    gf = GigaFrostCamera(
        "X02DA-CAM-GF2:", name="gf2", backend_url="http://xbl-daq-28:8080", auto_soft_enable=True
    )
    gf.wait_for_connection()
