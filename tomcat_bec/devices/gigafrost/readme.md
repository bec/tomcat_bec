# Gifgafrost camera at Tomcat

The GigaFrost camera IOC is a form from an ancient version of Helge's cameras.
As we're commissioning, the current folder also contains the standard DAQ client.
The ophyd implementation tries to balance between familiarity with the old 
**gfclient** pyepics library and the BEC/bluesky event model.

# Examples

A simple code example with soft triggering:
'''
d = {'ntotal':100000, 'nimages':3009, 'exposure':10.0, 'period':20.0, 'pixel_width':2016, 'pixel_height':2016}
gfc.configure(d)
gfc.stage()
for ii in range(10):
    gfc.trigger()
gfc.unstage()
'''


# Opening GigaFrost panel

The CaQtDM panel can be opened by:
'''
 caqtdm -macro "CAM=X02DA-CAM-GF2" X_X02DA_GIGAFROST_camControl_user.ui &
'''

# Opening PCO Edge panel

The CaQtDM panel can be opened by:
'''
caqtdm -macro "NAME=X02DA-CCDCAM2" CameraExpert.ui
'''
