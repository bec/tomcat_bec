# -*- coding: utf-8 -*-
"""
Copied utility class from gfclient

Created on Thu Jun 27 17:28:43 2024

@author: mohacsi_i
"""
# Not installed on BEC server
#import jsonschema

MIN_EXPOSURE_MS = 0.002
MAX_EXPOSURE_MS = 40

MIN_ROIX = 48
MAX_ROIX = 2016
STEP_ROIX = 48

MIN_ROIY = 4
MAX_ROIY = 2016
STEP_ROIY = 4

valid_roix = range(MIN_ROIX, MAX_ROIX + 1, STEP_ROIX)
valid_roiy = range(MIN_ROIY, MAX_ROIY + 1, STEP_ROIY)


def is_valid_url(url):
    """Basic URL validation"""
    # FIXME: do more checks?
    return url.startswith("http://")


def is_valid_exposure_ms(exp_t):
    """check if an exposure time e is valid for gigafrost

       exp_t: exposure time in milliseconds
    """
    return MIN_EXPOSURE_MS <= exp_t <= MAX_EXPOSURE_MS


def port2byte(port):
    """Post number and endianness conversion"""
    return [(port >> 8) & 0xFF, port & 0xFF]


def extend_header_table(table, mac, destination_ip, destination_port, source_port):
    """
        Extend the header table by a further entry.

        Parameters
        ----------
        table :
            The table to be extended
        mac :
            The mac address for the new entry
        destination_ip :
            The destination IP address for the new entry
        destination_port :
            The destination port for the new entry
        source_port :
            The source port for the new entry

        """
    table.extend(mac)
    table.extend(destination_ip)
    table.extend(port2byte(destination_port))
    table.extend(port2byte(source_port))
    return table


def is_valid_roi(roiy, roix):
    """ Validates ROI on GigaFrost"""
    return roiy in valid_roiy and roix in valid_roix


def _print_max_framerate(exposure, roix, roiy):
    """Prints the maximum GigaFrost framerate for a given ROI"""
    print(
        "roiy=%4i roix=%4i exposure=%6.3fms:  %8.1fHz"
        % (roiy, roix, exposure, max_framerate_hz(exposure, roix=roix, roiy=roiy))
    )


def print_max_framerate(exposure_ms=MIN_EXPOSURE_MS, shape="square"):
    """Prints the maximum GigaFrost framerate for a given exposure time"""
    valid_shapes = ["square", "landscape", "portrait"]

    if shape not in valid_shapes:
        raise ValueError("shape must be one of %s" % valid_shapes)
    if shape == "square":
        for px_r in valid_roix:
            _print_max_framerate(exposure_ms, px_r, px_r)

    if shape == "portrait":
        for px_x in valid_roix:
            _print_max_framerate(exposure_ms, roix=px_x, roiy=MAX_ROIY)

    if shape == "landscape":
        # valid_roix is a subset of valid_roiy.
        # Use the smaller set to get a more manageable amount of output lines
        for px_y in valid_roix:
            _print_max_framerate(exposure_ms, roix=MAX_ROIX, roiy=px_y)


def max_framerate_hz(exposure_ms=MIN_EXPOSURE_MS, roix=MAX_ROIX, roiy=MAX_ROIY, clk_mhz=62.5):
    """
    returns maximum achievable frame rate in auto mode in Hz

    Gerd Theidel wrote:
    Hallo zusammen,

    hier wie besprochen die Info zu den Zeiten.
    Im Anhang findet ihr ein Python Beispiel zur
    Berechnung der maximalen Framerate im auto trigger mode.
    Bei den anderen modes sollte man etwas darunter bleiben,
    wie auch im PCO Manual beschrieben.

    Zusammengefasst mit den aktuellen Konstanten:

    exposure_ms : 0.002 ... 40  (ms)
    clk_mhz     : 62.5 | 55.0 | 52.5 | 50.0  (MHz)

    t_readout = ( ((roi_x / 24) + 14) * (roi_y /  4) + 405) / (1e6 * clk_mhz)

    t_exp_sys = (exposure_ms / 1000.0) + (261 / (1e6 * clk_mhz))

    framerate = 1.0 / max(t_readout, t_exp_sys)

    Gruss,
     Gerd

    """
    # pylint: disable=invalid-name
    # pylint: disable=too-many-locals
    if exposure_ms < 0.002 or exposure_ms > 40:
        raise ValueError("exposure_ms not in interval [0.002, 40.]")

    valid_clock_values = [62.5, 55.0, 52.5, 50.0]
    if clk_mhz not in valid_clock_values:
        raise ValueError("clock rate not in %s" % valid_clock_values)

    # Constants
    PLB_IMG_SENS_COUNT_X_OFFS = 2
    PLB_IMG_SENS_COUNT_Y_OFFS = 1

    # Constant Clock Cycles
    CC_T2 = 88  # 99
    CC_T3 = 125
    CC_T4 = 1
    CC_T10 = 2
    CC_T11 = 4
    CC_T5_MINUS_T11 = 20
    CC_T15_MINUS_T10 = 3
    CC_TR3 = 1
    CC_T13_MINUS_TR3 = 2
    CC_150NS = 7  # additional delay through states
    CC_DELAY_BEFORE_RESET = 4  # at least 50 ns
    CC_ADDITIONAL_TSYS = 10
    CC_PIX_RESET_LENGTH = 40  # at least 40 CLK Cycles at 62.5 MHz
    CC_COUNT_X_MAX = 84

    CC_ROW_OVERHEAD_TIME = 11 + CC_TR3 + CC_T13_MINUS_TR3
    CC_FRAME_OVERHEAD_TIME = (
        8 + CC_T15_MINUS_T10 + CC_T10 + CC_T2 + CC_T3 + CC_T4 + CC_T5_MINUS_T11 + CC_T11
    )
    CC_INTEGRATION_START_DELAY = (
        CC_COUNT_X_MAX
        + CC_ROW_OVERHEAD_TIME
        + CC_DELAY_BEFORE_RESET
        + CC_PIX_RESET_LENGTH
        + CC_150NS
        + 5
    )
    CC_TIME_TSYS = CC_FRAME_OVERHEAD_TIME + CC_ADDITIONAL_TSYS

    # 12 pixel blocks per quarter
    roix = max(((roix + 47) / 48) * 48, PLB_IMG_SENS_COUNT_X_OFFS * 24)
    # 2 line blocks per quarter
    roiy = max(((roiy + 3) / 4) * 4, PLB_IMG_SENS_COUNT_Y_OFFS * 4)

    #  print("CC_INTEGRATION_START_DELAY + CC_FRAME_OVERHEAD_TIME",
    #         CC_INTEGRATION_START_DELAY + CC_FRAME_OVERHEAD_TIME)
    #  print("CC_ROW_OVERHEAD_TIME",CC_ROW_OVERHEAD_TIME)
    #  print("CC_TIME_TSYS",CC_TIME_TSYS)

    t_readout = (
        CC_INTEGRATION_START_DELAY
        + CC_FRAME_OVERHEAD_TIME
        + ((roix / 24) + CC_ROW_OVERHEAD_TIME) * (roiy / 4)
    ) / (1e6 * clk_mhz)
    t_exp_sys = (exposure_ms / 1000.0) + (CC_TIME_TSYS / (1e6 * clk_mhz))

    #  with constants as of time of writing:
    #  t_readout = ( ((roix / 24) + 14) * (roiy /  4) + 405) / (1e6 * clk_mhz)
    #  t_exp_sys = (exposure_ms / 1000.0) + (261 / (1e6 * clk_mhz))

    framerate = 1.0 / max(t_readout, t_exp_sys)

    return framerate


layoutSchema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "patternProperties": {
        "^[a-zA-Z0-9_.-]*$": {
            "type": "object",
            "required": [
                "writer",
                "DaflClient",
                "zmq_stream",
                "live_preview",
                "ioc_name",
                "description",
            ],
            "properties": {
                "writer": {"type": "string"},
                "DaflClient": {"type": "string"},
                "zmq_stream": {"type": "string"},
                "live_preview": {"type": "string"},
                "ioc_name": {"type": "string"},
                "description": {"type": "string"},
            },
        }
    },
    "additionalProperties": False,
}


def validate_json(json_data):
    """Silently validate a JSON data according to the predefined schema"""
    #try:
    #    jsonschema.validate(instance=json_data, schema=layoutSchema)
    #except jsonschema.exceptions.ValidationError:
    #    return False
    return True
