# -*- coding: utf-8 -*-
"""
Standard DAQ control interface module through the websocket API

Created on Thu Jun 27 17:28:43 2024

@author: mohacsi_i
"""
import json
from time import sleep
from threading import Thread
import requests
import os

from ophyd import Signal, Component, Kind
from ophyd.status import SubscriptionStatus
from websockets.sync.client import connect, ClientConnection
from websockets.exceptions import ConnectionClosedOK, ConnectionClosedError

from ophyd_devices.interfaces.base_classes.psi_detector_base import PSIDetectorBase as PSIDeviceBase
from ophyd_devices.interfaces.base_classes.psi_detector_base import (
    CustomDetectorMixin as CustomDeviceMixin,
)
from bec_lib import bec_logger

logger = bec_logger.logger


class StdDaqMixin(CustomDeviceMixin):
    # pylint: disable=protected-access
    _mon = None

    def on_stage(self) -> None:
        """Configuration and staging

        In the BEC model ophyd devices must fish out their own configuration from the 'scaninfo'.
        I.e. they need to know which parameters are relevant for them at each scan.

        NOTE: Tomcat might use multiple cameras with their own separate DAQ instances.
        """
        # Fish out our configuration from scaninfo (via explicit or generic addressing)
        # NOTE: Scans don't have to fully configure the device
        d = {}
        if "kwargs" in self.parent.scaninfo.scan_msg.info:
            scanargs = self.parent.scaninfo.scan_msg.info["kwargs"]
            if "image_width" in scanargs and scanargs["image_width"] is not None:
                d["image_width"] = scanargs["image_width"]
            if "image_height" in scanargs and scanargs["image_height"] is not None:
                d["image_height"] = scanargs["image_height"]
            if "nr_writers" in scanargs and scanargs["nr_writers"] is not None:
                d["nr_writers"] = scanargs["nr_writers"]
            if "file_path" in scanargs and scanargs["file_path"] is not None:
                self.parent.file_path.set(scanargs["file_path"].replace("data", "gpfs")).wait()
                print(scanargs["file_path"])
                if os.path.isdir(scanargs["file_path"]):
                    print("isdir")
                    pass
                else:
                    print("creating")
                    try:
                        os.makedirs(scanargs["file_path"], 0o777)
                        os.system("chmod -R 777 " + scanargs["base_path"])
                    except:
                        print("Problem with creating folder")
            if "file_prefix" in scanargs and scanargs["file_prefix"] != None:
                print(scanargs["file_prefix"])
                self.parent.file_prefix.set(scanargs["file_prefix"]).wait()

            if "daq_num_points" in scanargs:
                d["num_points_total"] = scanargs["daq_num_points"]
            else:
                # Try to figure out number of points
                num_points = 1
                points_valid = False
                if "steps" in scanargs and scanargs["steps"] is not None:
                    num_points *= scanargs["steps"]
                    points_valid = True
                if "exp_burst" in scanargs and scanargs["exp_burst"] is not None:
                    num_points *= scanargs["exp_burst"]
                    points_valid = True
                if "repeats" in scanargs and scanargs["repeats"] is not None:
                    num_points *= scanargs["repeats"]
                    points_valid = True
                if points_valid:
                    d["num_points_total"] = num_points

        # Perform bluesky-style configuration
        if len(d) > 0:
            # Configure new run (will restart the stdDAQ)
            logger.warning(f"[{self.parent.name}] stdDAQ needs reconfiguring with:\n{d}")
            self.parent.configure(d=d)
            # Wait for REST API to kill the DAQ
            sleep(0.5)

        # Try to start a new run (reconnects)
        self.parent.bluestage()
        # And start status monitoring
        self._mon = Thread(target=self.monitor, daemon=True)
        self._mon.start()

    def on_unstage(self):
        """Stop a running acquisition and close connection"""
        print("Creating virtual dataset")
        self.parent.create_virtual_dataset()
        self.parent.blueunstage()

    def on_stop(self):
        """Stop a running acquisition and close connection"""
        self.parent.blueunstage()

    def monitor(self) -> None:
        """Monitor status messages while connection is open. This will block the reply monitoring
        to calling unstage() might throw. Status updates are sent every 1 seconds, but finishing
        acquisition means StdDAQ will close connection, so there's no idle state polling.
        """
        try:
            sleep(0.2)
            for msg in self.parent._wsclient:
                message = json.loads(msg)
                self.parent.runstatus.put(message["status"], force=True)
                # logger.info(f"[{self.parent.name}] Pushed status: {message['status']}")
        except (ConnectionClosedError, ConnectionClosedOK, AssertionError):
            # Libraty throws theese after connection is closed
            return
        except Exception as ex:
            logger.warning(f"[{self.parent.name}] Exception in polling: {ex}")
            return
        finally:
            self._mon = None


class StdDaqClient(PSIDeviceBase):
    """StdDaq API

    This class combines the new websocket and REST interfaces of the stdDAQ. The websocket
    interface starts and stops the acquisition and provides status, while the REST interface
    can read and write the JSON configuration file. The stdDAQ needs to restart all services
    to reconfigure with a new config, which might corrupt
    the currently written files (fix is underway).

    Example:
    ```
    daq = StdDaqClient(name="daq", ws_url="ws://xbl-daq-29:8080", rest_url="http://xbl-daq-29:5000")
    ```
    """

    # pylint: disable=too-many-instance-attributes
    custom_prepare_cls = StdDaqMixin
    USER_ACCESS = [
        "set_daq_config",
        "get_daq_config",
        "nuke",
        "connect",
        "message",
        "state",
        "bluestage",
        "blueunstage",
    ]
    _wsclient = None

    # Status attributes
    ws_url = Component(Signal, kind=Kind.config, metadata={"write_access": False})
    runstatus = Component(
        Signal, value="unknown", kind=Kind.normal, metadata={"write_access": False}
    )
    num_images = Component(Signal, value=10000, kind=Kind.config)
    file_path = Component(Signal, value="/gpfs/test/test-beamline", kind=Kind.config)
    file_prefix = Component(Signal, value="file", kind=Kind.config)
    # Configuration attributes
    rest_url = Component(Signal, kind=Kind.config, metadata={"write_access": False})
    cfg_detector_name = Component(Signal, kind=Kind.config)
    cfg_detector_type = Component(Signal, kind=Kind.config)
    cfg_bit_depth = Component(Signal, kind=Kind.config)
    cfg_pixel_height = Component(Signal, kind=Kind.config)
    cfg_pixel_width = Component(Signal, kind=Kind.config)
    cfg_nr_writers = Component(Signal, kind=Kind.config)

    def __init__(
        self,
        prefix="",
        *,
        name,
        kind=None,
        read_attrs=None,
        configuration_attrs=None,
        parent=None,
        device_manager=None,
        ws_url: str = "ws://localhost:8080",
        rest_url: str = "http://localhost:5000",
        data_source_name=None,
        **kwargs,
    ) -> None:
        super().__init__(
            prefix=prefix,
            name=name,
            kind=kind,
            read_attrs=read_attrs,
            configuration_attrs=configuration_attrs,
            parent=parent,
            device_manager=device_manager,
            **kwargs,
        )
        self.ws_url.set(ws_url, force=True).wait()
        self.rest_url.set(rest_url, force=True).wait()
        self.data_source_name = data_source_name

        # Connect to the DAQ and initialize values
        try:
            self.get_daq_config(update=True)
        except Exception as ex:
            logger.error(f"Failed to connect to the stdDAQ REST API\n{ex}")

    def connect(self) -> ClientConnection:
        """Connect to the StdDAQ's websockets interface

        StdDAQ may reject connection for a few seconds after restart, or when
        it wants so if it fails, wait a bit and try to connect again.
        """
        num_retry = 0
        while num_retry < 5:
            try:
                logger.debug(f"[{self.name}] Connecting to stdDAQ at {self.ws_url.get()}")
                connection = connect(self.ws_url.get())
                logger.debug(f"[{self.name}] Connected to stdDAQ after {num_retry} tries")
                return connection
            except ConnectionRefusedError:
                num_retry += 1
                sleep(2)
        raise ConnectionRefusedError("The stdDAQ websocket interface refused connection 5 times.")

    def message(self, message: dict, timeout=1, wait_reply=True, client=None) -> None | str:
        """Send a message to the StdDAQ and receive a reply

        Note: finishing acquisition means StdDAQ will close connection, so
        there's no idle state polling.
        """
        # Prepare message
        msg = json.dumps(message) if isinstance(message, dict) else str(message)

        # Connect if client was destroyed
        if self._wsclient is None:
            self._wsclient = self.connect()

        # Send message (reopen connection if needed)
        msg = json.dumps(message) if isinstance(message, dict) else str(message)
        try:
            self._wsclient.send(msg)
        except (ConnectionClosedError, ConnectionClosedOK, AttributeError) as ex:
            # Re-connect if the connection was closed
            self._wsclient = self.connect()
            self._wsclient.send(msg)

        # Wait for reply
        reply = None
        if wait_reply:
            try:
                reply = self._wsclient.recv(timeout)
                return reply
            except (ConnectionClosedError, ConnectionClosedOK) as ex:
                self._wsclient = None
                logger.error(f"[{self.name}] WS connection was closed before reply: {ex}")
            except (TimeoutError, RuntimeError) as ex:
                logger.error(f"[{self.name}] Error in receiving ws reply: {ex}")
        return reply

    def configure(self, d: dict = None):
        """Configure the next scan with the stdDAQ

        Parameters as 'd' dictionary, the default is unchanged.
        ----------------------------
        num_points_total : int, optional
            Number of images to be taken during each scan. Set to -1 for an unlimited number of
            images (limited by the ringbuffer size and backend speed).
        file_path: str, optional
            File path to save the data, usually GPFS.
        image_width : int, optional
            ROI size in the x-direction [pixels].
        image_height : int, optional
            ROI size in the y-direction [pixels].
        bit_depth: int, optional
            Image bit depth for cameras that can change it [int].
        nr_writers: int, optional
            Number of writers [int].
        """

        # Configuration parameters
        if "image_width" in d and d["image_width"] != None:
            self.cfg_pixel_width.set(d["image_width"]).wait()
        if "image_height" in d and d["image_height"] != None:
            self.cfg_pixel_height.set(d["image_height"]).wait()
        if "bit_depth" in d:
            self.cfg_bit_depth.set(d["bit_depth"]).wait()
        if "nr_writers" in d and d["nr_writers"] != None:
            self.cfg_nr_writers.set(d["nr_writers"]).wait()
        # Run parameters
        if "num_points_total" in d:
            self.num_images.set(d["num_points_total"]).wait()

        # Restart the DAQ if resolution changed
        cfg = self.get_daq_config()
        if (
            cfg["image_pixel_height"] != self.cfg_pixel_height.get()
            or cfg["image_pixel_width"] != self.cfg_pixel_width.get()
            or cfg["bit_depth"] != self.cfg_bit_depth.get()
            or cfg["number_of_writers"] != self.cfg_nr_writers.get()
        ):

            # Stop if current status is not idle
            if self.state() != "idle":
                logger.warning(f"[{self.name}] stdDAQ reconfiguration might corrupt files")

            # Update retrieved config
            cfg["image_pixel_height"] = int(self.cfg_pixel_height.get())
            cfg["image_pixel_width"] = int(self.cfg_pixel_width.get())
            cfg["bit_depth"] = int(self.cfg_bit_depth.get())
            cfg["number_of_writers"] = int(self.cfg_nr_writers.get())
            self.set_daq_config(cfg)
            sleep(1)
            self.get_daq_config(update=True)

    def bluestage(self):
        """Stages the stdDAQ

        Opens a new connection to the stdDAQ, sends the start command with
        the current configuration. It waits for the first reply and checks
        it for obvious failures.
        """
        # Can't stage into a running exposure
        if self.state() != "idle":
            raise RuntimeError(f"[{self.name}] stdDAQ can't stage from state: {self.state()}")

        # Must make sure that image size matches the data source
        if self.data_source_name is not None:
            cam_img_w = self.device_manager.devices[self.data_source_name].cfgRoiX.get()
            cam_img_h = self.device_manager.devices[self.data_source_name].cfgRoiY.get()
            daq_img_w = self.cfg_pixel_width.get()
            daq_img_h = self.cfg_pixel_height.get()

            if not (daq_img_w == cam_img_w and daq_img_h == cam_img_h):
                raise RuntimeError(
                    f"[{self.name}] stdDAQ image resolution ({daq_img_w} , {daq_img_h}) does not match camera with ({cam_img_w} , {cam_img_h})"
                )
            else:
                logger.warning(
                    f"[{self.name}] stdDAQ image resolution ({daq_img_w} , {daq_img_h}) matches camera with ({cam_img_w} , {cam_img_h})"
                )

        file_path = self.file_path.get()
        num_images = self.num_images.get()
        file_prefix = self.file_prefix.get()
        print(file_prefix)

        # New connection
        self._wsclient = self.connect()
        message = {
            "command": "start",
            "path": file_path,
            "file_prefix": file_prefix,
            "n_image": num_images,
        }
        reply = self.message(message)

        if reply is not None:
            reply = json.loads(reply)
            self.runstatus.set(reply["status"], force=True).wait()
            logger.info(f"[{self.name}] Start DAQ reply:  {reply}")

            # Give it more time to reconfigure
            if reply["status"] in ("rejected"):
                # FIXME: running exposure is a nogo
                if reply["reason"] == "driver is busy!":
                    raise RuntimeError(
                        f"[{self.name}] Start stdDAQ command rejected: already running"
                    )
                else:
                    # Give it more time to consolidate
                    sleep(1)
            else:
                # Success!!!
                print(f"[{self.name}] Started stdDAQ in: {reply['status']}")
                return

        raise RuntimeError(
            f"[{self.name}] Failed to start the stdDAQ in 1 tries, reason: {reply['reason']}"
        )

    def blueunstage(self):
        """Unstages the stdDAQ

        Opens a new connection to the stdDAQ, sends the stop command and
        waits for the idle state.
        """
        ii = 0
        while ii < 10:
            # Stop the DAQ (will close connection) - reply is always "success"
            self._wsclient = self.connect()
            self.message({"command": "stop_all"}, wait_reply=False)

            # Let it consolidate
            sleep(0.2)

            # Check final status (from new connection)
            self._wsclient = self.connect()
            reply = self.message({"command": "status"})
            if reply is not None:
                logger.info(f"[{self.name}] DAQ status reply: {reply}")
                reply = json.loads(reply)

                if reply["status"] in ("idle", "error"):
                    # Only 'idle' state accepted
                    print(f"DAQ stopped on try {ii}")
                    return
                elif reply["status"] in ("stop"):
                    # Give it more time to stop
                    sleep(0.5)
                elif ii >= 6:
                    raise RuntimeError(f"Failed to stop StdDAQ: {reply}")
            ii += 1
        raise RuntimeError(f"Failed to stop StdDAQ in time")

    ##########################################################################
    # Bluesky flyer interface
    def complete(self) -> SubscriptionStatus:
        """Wait for current run. Must end in status 'file_saved'."""

        def is_running(*args, value, timestamp, **kwargs):
            result = value in ["idle", "file_saved", "error"]
            return result

        status = SubscriptionStatus(self.runstatus, is_running, settle_time=0.5)
        return status

    def get_daq_config(self, update=False) -> dict:
        """Read the current configuration from the DAQ"""
        r = requests.get(self.rest_url.get() + "/api/config/get", params={"user": "ioc"}, timeout=2)
        if r.status_code != 200:
            raise ConnectionError(f"[{self.name}] Error {r.status_code}:\t{r.text}")
        cfg = r.json()

        if update:
            self.cfg_detector_name.set(cfg["detector_name"]).wait()
            self.cfg_detector_type.set(cfg["detector_type"]).wait()
            self.cfg_bit_depth.set(cfg["bit_depth"]).wait()
            self.cfg_pixel_height.set(cfg["image_pixel_height"]).wait()
            self.cfg_pixel_width.set(cfg["image_pixel_width"]).wait()
            self.cfg_nr_writers.set(cfg["number_of_writers"]).wait()
        return cfg

    def set_daq_config(self, config, settle_time=1):
        """Write a full configuration to the DAQ"""
        url = self.rest_url.get() + "/api/config/set"
        r = requests.post(
            url,
            params={"user": "ioc"},
            json=config,
            timeout=2,
            headers={"Content-Type": "application/json"},
        )
        if r.status_code != 200:
            raise ConnectionError(f"[{self.name}] Error {r.status_code}:\t{r.text}")
        # Wait for service to restart (and connect to make sure)
        # sleep(settle_time)
        self.connect()
        return r.json()

    def create_virtual_dataset(self):
        """Combine the stddaq written files in a given folder in an interleaved
        h5 virtual dataset
        """
        url = self.rest_url.get() + "/api/h5/create_interleaved_vds"
        file_path = self.file_path.get()
        file_prefix = self.file_prefix.get()

        r = requests.post(
            url,
            params={"user": "ioc"},
            json={
                "base_path": file_path,
                "file_prefix": file_prefix,
                "output_file": file_prefix.rstrip("_") + ".h5",
            },
            timeout=2,
            headers={"Content-type": "application/json"},
        )

    def nuke(self, restarttime=5):
        """Reconfigures the stdDAQ to restart the services. This causes
        systemd to kill the current DAQ service and restart it with the same
        configuration. Which might corrupt the currently written file...
        """
        cfg = self.get_daq_config()
        self.set_daq_config(cfg)
        sleep(restarttime)

    def state(self) -> str | None:
        """Querry the current system status"""
        try:
            wsclient = self.connect()
            wsclient.send(json.dumps({"command": "status"}))
            r = wsclient.recv(timeout=1)
            r = json.loads(r)
            return r["status"]
        except ConnectionRefusedError:
            raise


# Automatically connect to microXAS testbench if directly invoked
if __name__ == "__main__":
    daq = StdDaqClient(
        name="daq", ws_url="ws://sls-daq-001:8080", rest_url="http://sls-daq-001:5000"
    )
    daq.wait_for_connection()
