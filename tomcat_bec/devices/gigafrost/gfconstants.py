# -*- coding: utf-8 -*-
"""
Copied utility class from gfclient

Created on Thu Jun 27 17:28:43 2024

@author: mohacsi_i
"""
from enum import IntEnum


gf_valid_enable_modes = ("soft", "external", "soft+ext", "always")
gf_valid_exposure_modes = ("external", "timer", "soft")
gf_valid_trigger_modes = ("auto", "external", "timer", "soft")
gf_valid_fix_nframe_modes = ("off", "start", "end", "start+end")


# STATUS
class GfStatus(IntEnum):
    """Operation states for GigaFrost Ophyd device"""
    NEW = 1
    INITIALIZED = 2
    ACQUIRING = 3
    CONFIGURED = 4
    STOPPED = 5
    INIT = 6


# BACKEND ADDRESSES
BE1_DAFL_CLIENT = "http://xbl-daq-33:8080"
BE1_NORTH_MAC = [0x94, 0x40, 0xC9, 0xB4, 0xB8, 0x00]
BE1_SOUTH_MAC = [0x94, 0x40, 0xC9, 0xB4, 0xA8, 0xD8]
BE1_NORTH_IP = [10, 4, 0, 102]
BE1_SOUTH_IP = [10, 0, 0, 102]
BE2_DAFL_CLIENT = "http://xbl-daq-23:8080"
BE2_NORTH_MAC = [0x24, 0xBE, 0x05, 0xAC, 0x03, 0x62]
BE2_SOUTH_MAC = [0x24, 0xBE, 0x05, 0xAC, 0x03, 0x72]
BE2_NORTH_IP = [10, 4, 0, 100]
BE2_SOUTH_IP = [10, 0, 0, 100]
BE3_DAFL_CLIENT = "http://xbl-daq-26:8080"
BE3_NORTH_MAC = [0x50, 0x65, 0xF3, 0x81, 0x66, 0x51]
BE3_SOUTH_MAC = [0x50, 0x65, 0xF3, 0x81, 0xD5, 0x31]  # ens4
BE3_NORTH_IP = [10, 4, 0, 101]
BE3_SOUTH_IP = [10, 0, 0, 101]

# Backend for MicroXAS test-stand
BE999_DAFL_CLIENT = "http://sls-daq-001:8080"
BE999_SOUTH_MAC = [0x88, 0xE9, 0xA4, 0xC2, 0xA6, 0x09]
BE999_NORTH_MAC = [0x88, 0xE9, 0xA4, 0xC2, 0xA6, 0x08]
BE999_NORTH_IP = [10, 4, 0, 102]
BE999_SOUTH_IP = [10, 0, 0, 102]

# GF Names
GF1 = "gf1"
GF2 = "gf2"
GF3 = "gf3"
