# -*- coding: utf-8 -*-
"""
Ophyd device for the Aerotech Automation1 IOC's generic interfaces

@author: mohacsi_i
"""
import numpy as np
from ophyd import Component, Device, EpicsMotor, EpicsSignal, EpicsSignalRO, Kind

from bec_lib import bec_logger
logger = bec_logger.logger


class EpicsPassiveRO(EpicsSignalRO):
    """Small helper class to read PVs that need to be processed first."""

    def __init__(self, read_pv, *, string=False, name=None, **kwargs):
        super().__init__(read_pv, string=string, name=name, **kwargs)
        self._proc = EpicsSignal(read_pv + ".PROC", kind=Kind.omitted, put_complete=True)

    def wait_for_connection(self, *args, **kwargs):
        super().wait_for_connection(*args, **kwargs)
        self._proc.wait_for_connection(*args, **kwargs)

    def get(self, *args, **kwargs):
        self._proc.set(1).wait()
        return super().get(*args, **kwargs)

    # @property
    # def value(self):
    #     return super().value


class aa1Controller(Device):
    """Ophyd proxy class for the Aerotech Automation 1's core controller functionality"""

    # ToDo: Add error subscription
    controllername = Component(EpicsSignalRO, "NAME", kind=Kind.config)
    serialnumber = Component(EpicsSignalRO, "SN", kind=Kind.config)
    apiversion = Component(EpicsSignalRO, "API_VERSION", kind=Kind.config)
    axiscount = Component(EpicsSignalRO, "AXISCOUNT", kind=Kind.config)
    taskcount = Component(EpicsSignalRO, "TASKCOUNT", kind=Kind.config)
    fastpoll = Component(EpicsSignalRO, "POLLTIME", auto_monitor=True, kind=Kind.normal)
    slowpoll = Component(EpicsSignalRO, "DRVPOLLTIME", auto_monitor=True, kind=Kind.normal)
    errno = Component(EpicsSignalRO, "ERRNO", auto_monitor=True, kind=Kind.normal)
    errnmsg = Component(EpicsSignalRO, "ERRMSG", auto_monitor=True, kind=Kind.normal)
    _set_ismc = Component(EpicsSignal, "SET", put_complete=True, kind=Kind.omitted)

    USER_ACCESS = ["reset"]

    def reset(self):
        """ Resets the Automation1 iSMC reloading the default configuration"""
        self._set_ismc.set(3).wait()


class aa1GlobalVariables(Device):
    """Global variables

    This class provides a low-level interface to directly read/write global
    variables on the Automation1 controller. These variables are accesible
    from script files and are thus a convenient way to interface with the
    outside word.

    Read operations take as input the memory address and the size
    Write operations work with the memory address and value

    Examples:
    ----------
    '''
    var = aa1GlobalVariables(AA1_IOC_NAME+":VAR:", name="var")
    var.wait_for_connection()
    ret = var.read_int(42)
    var.write_float(1000, np.random.random(1024))
    ret_arr = var.read_float(1000, 1024)
    '''
    """
    USER_ACCESS = ['read_int', 'write_int', 'read_float', 'write_float', 'read_string', 'write_string']
    # Available capacity
    num_real = Component(EpicsSignalRO, "NUM-REAL_RBV", kind=Kind.config)
    num_int = Component(EpicsSignalRO, "NUM-INT_RBV", kind=Kind.config)
    num_string = Component(EpicsSignalRO, "NUM-STRING_RBV", kind=Kind.config)

    # Read-write interface
    integer_addr = Component(EpicsSignal, "INT-ADDR", kind=Kind.omitted, put_complete=True)
    integer_size = Component(EpicsSignal, "INT-SIZE", kind=Kind.omitted, put_complete=True)
    integer = Component(EpicsSignal, "INT", kind=Kind.omitted, put_complete=True)
    integer_rb = Component(EpicsPassiveRO, "INT-RBV", kind=Kind.normal)
    integerarr = Component(EpicsSignal, "INTARR", kind=Kind.omitted, put_complete=True)
    integerarr_rb = Component(EpicsPassiveRO, "INTARR-RBV", kind=Kind.normal)

    real_addr = Component(EpicsSignal, "REAL-ADDR", kind=Kind.omitted, put_complete=True)
    real_size = Component(EpicsSignal, "REAL-SIZE", kind=Kind.omitted, put_complete=True)
    real = Component(EpicsSignal, "REAL", kind=Kind.omitted, put_complete=True)
    real_rb = Component(EpicsPassiveRO, "REAL-RBV", kind=Kind.normal)
    realarr = Component(EpicsSignal, "REALARR", kind=Kind.omitted, put_complete=True)
    realarr_rb = Component(EpicsPassiveRO, "REALARR-RBV", kind=Kind.normal)

    string_addr = Component(EpicsSignal, "STRING-ADDR", kind=Kind.omitted, put_complete=True)
    string = Component(EpicsSignal, "STRING", string=True, kind=Kind.omitted, put_complete=True)
    string_rb = Component(EpicsPassiveRO, "STRING-RBV", string=True, kind=Kind.normal)

    def read_int(self, address: int, size: int = None) -> int:
        """Read a 64-bit integer global variable

        Method to reads scalar and array global integer variables.

        Parameters:
        ------------
        address :   Memory (start) address of the global integer.
        size    :   Array size, set to 0 or None for scalar [default=None]
        """
        if address > self.num_int.get():
            raise RuntimeError("Integer address {address} is out of range")

        if size is None or size == 0:
            self.integer_addr.set(address).wait()
            return self.integer_rb.get()

        self.integer_addr.set(address).wait()
        self.integer_size.set(size).wait()
        return self.integerarr_rb.get()

    def write_int(self, address: int, value, settle_time=0.1) -> None:
        """Write a 64-bit integer global variable

        Method to write scalar or array global integer variables.

        Parameters:
        ------------
        address :   Memory (start) address of the global integer.
        value   :   Scalar, list, tuple or ndarray of numbers.
        """
        if address > self.num_int.get():
            raise RuntimeError("Integer address {address} is out of range")

        if isinstance(value, (int, float)):
            self.integer_addr.set(address).wait()
            self.integer.set(value, settle_time=settle_time).wait()
        elif isinstance(value, np.ndarray):
            self.integer_addr.set(address).wait()
            self.integerarr.set(value, settle_time=settle_time).wait()
        elif isinstance(value, (list, tuple)):
            value = np.array(value, dtype=np.int32)
            self.integer_addr.set(address).wait()
            self.integerarr.set(value, settle_time=settle_time).wait()
        else:
            raise RuntimeError("Unsupported integer value type: {type(value)}")

    def read_float(self, address: int, size: int = None) -> float:
        """Read a 64-bit double global variable"""
        if address > self.num_real.get():
            raise RuntimeError("Floating point address {address} is out of range")

        if size is None:
            self.real_addr.set(address).wait()
            return self.real_rb.get()

        self.real_addr.set(address).wait()
        self.real_size.set(size).wait()
        return self.realarr_rb.get()

    def write_float(self, address: int, value, settle_time=0.1) -> None:
        """Write a 64-bit float global variable"""
        if address > self.num_real.get():
            raise RuntimeError("Float address {address} is out of range")

        if isinstance(value, (int, float)):
            self.real_addr.set(address).wait()
            self.real.set(value, settle_time=settle_time).wait()
        elif isinstance(value, np.ndarray):
            self.real_addr.set(address).wait()
            self.realarr.set(value, settle_time=settle_time).wait()
        elif isinstance(value, (list, tuple)):
            value = np.array(value)
            self.real_addr.set(address).wait()
            self.realarr.set(value, settle_time=settle_time).wait()
        else:
            raise RuntimeError("Unsupported float value type: {type(value)}")

    def read_string(self, address: int) -> str:
        """Read a string global variable

        Method to read a sting global variable. Standard Automation1 strings
        are 256 bytes long (255 sharacter).

        Parameters:
        ------------
        address :   Memory address of the global string.
        """
        if address > self.num_string.get():
            raise RuntimeError("String address {address} is out of range")

        self.string_addr.set(address).wait()
        return self.string_rb.get()

    def write_string(self, address: int, value, settle_time=0.1) -> None:
        """Write a string global variable

        Method to write a maximum 255 character string global integer variable.

        Parameters:
        ------------
        address :   Memory address of the global string.
        value   :   The string to write.
        """
        if address > self.num_string.get():
            raise RuntimeError("Integer address {address} is out of range")
        if len(value) > 255:
            raise RuntimeError(f"Max string length is 255 characters, tried {len(value)}")

        if isinstance(value, str):
            self.string_addr.set(address).wait()
            self.string.set(value, settle_time=settle_time).wait()
        else:
            raise RuntimeError("Unsupported string value type: {type(value)}")


class aa1GlobalVariableBindings(Device):
    """Polled global variables

    This class provides an interface to read/write the first few global variables
    on the Automation1 controller. These variables can be directly set and are
    continuously polled and are thus a convenient way to interface scripts with
    the outside word.
    """

    int0 = Component(EpicsSignalRO, "INT0_RBV", auto_monitor=True, name="int0", kind=Kind.hinted)
    int1 = Component(EpicsSignalRO, "INT1_RBV", auto_monitor=True, name="int1", kind=Kind.hinted)
    int2 = Component(EpicsSignalRO, "INT2_RBV", auto_monitor=True, name="int2", kind=Kind.hinted)
    int3 = Component(EpicsSignalRO, "INT3_RBV", auto_monitor=True, name="int3", kind=Kind.hinted)
    int8 = Component(EpicsSignal, "INT8_RBV", put_complete=True, write_pv="INT8", auto_monitor=True, name="int8")
    int9 = Component(EpicsSignal, "INT9_RBV", put_complete=True, write_pv="INT9", auto_monitor=True, name="int9")
    int10 = Component(EpicsSignal, "INT10_RBV", put_complete=True, write_pv="INT10", auto_monitor=True, name="int10")
    int11 = Component(EpicsSignal, "INT11_RBV", put_complete=True, write_pv="INT11", auto_monitor=True, name="int11")

    float0 = Component(EpicsSignalRO, "REAL0_RBV", auto_monitor=True, name="float0")
    float1 = Component(EpicsSignalRO, "REAL1_RBV", auto_monitor=True, name="float1")
    float2 = Component(EpicsSignalRO, "REAL2_RBV", auto_monitor=True, name="float2")
    float3 = Component(EpicsSignalRO, "REAL3_RBV", auto_monitor=True, name="float3")
    float16 = Component(EpicsSignal, "REAL16_RBV", write_pv="REAL16", put_complete=True, auto_monitor=True, name="float16")
    float17 = Component(EpicsSignal, "REAL17_RBV", write_pv="REAL17", put_complete=True, auto_monitor=True, name="float17")
    float18 = Component(EpicsSignal, "REAL18_RBV", write_pv="REAL18", put_complete=True, auto_monitor=True, name="float18")
    float19 = Component(EpicsSignal, "REAL19_RBV", write_pv="REAL19", put_complete=True, auto_monitor=True, name="float19")

    # BEC LiveTable crashes on non-numeric values
    str0 = Component(EpicsSignalRO, "STR0_RBV", auto_monitor=True, string=True, name="str0")
    str1 = Component(EpicsSignalRO, "STR1_RBV", auto_monitor=True, string=True, name="str1")
    str4 = Component(EpicsSignal, "STR4_RBV", put_complete=True, string=True, auto_monitor=True, write_pv="STR4", name="str4")
    str5 = Component(EpicsSignal, "STR5_RBV", put_complete=True, string=True, auto_monitor=True, write_pv="STR5", name="str5")


class aa1AxisIo(Device):
    """Analog / digital Input-Output

    This class provides convenience wrappers around the Aerotech API's axis
    specific IO functionality. Note that this is a low-speed API, actual work
    should be done in AeroScript. Only one pin can be writen directly but
    several can be polled!
    """
    USER_ACCESS = ['set_analog', 'set_digital']

    ai0 = Component(EpicsSignalRO, "AI0-RBV", auto_monitor=True, kind=Kind.hinted)
    ai1 = Component(EpicsSignalRO, "AI1-RBV", auto_monitor=True, kind=Kind.hinted)
    ai2 = Component(EpicsSignalRO, "AI2-RBV", auto_monitor=True, kind=Kind.hinted)
    ai3 = Component(EpicsSignalRO, "AI3-RBV", auto_monitor=True, kind=Kind.hinted)
    ao0 = Component(EpicsSignalRO, "AO0-RBV", auto_monitor=True, kind=Kind.hinted)
    ao1 = Component(EpicsSignalRO, "AO1-RBV", auto_monitor=True, kind=Kind.hinted)
    ao2 = Component(EpicsSignalRO, "AO2-RBV", auto_monitor=True, kind=Kind.hinted)
    ao3 = Component(EpicsSignalRO, "AO3-RBV", auto_monitor=True, kind=Kind.hinted)
    di0 = Component(EpicsSignalRO, "DI0-RBV", auto_monitor=True, kind=Kind.hinted)
    do0 = Component(EpicsSignalRO, "DO0-RBV", auto_monitor=True, kind=Kind.hinted)

    ao_addr = Component(EpicsSignal, "AO-ADDR", put_complete=True, kind=Kind.config)
    ao = Component(EpicsSignal, "AO-RBV", write_pv="AO", auto_monitor=True, kind=Kind.hinted)

    do_addr = Component(EpicsSignal, "DO-ADDR", put_complete=True, kind=Kind.config)
    do = Component(EpicsSignal, "DO-RBV", write_pv="DO", auto_monitor=True, kind=Kind.hinted)

    def set_analog(self, pin: int, value: float, settle_time=0.05):
        """ Set an analog output pin"""
        # Set the address
        self.ao_addr.set(pin).wait()
        # Set the voltage
        self.ao.set(value, settle_time=settle_time).wait()

    def set_digital(self, pin: int, value: int, settle_time=0.05):
        """ Set a digital output pin"""
        # Set the address
        self.do_addr.set(pin).wait()
        # Set the voltage
        self.do.set(value, settle_time=settle_time).wait()


# Automatically start simulation if directly invoked
if __name__ == "__main__":
    AA1_IOC_NAME = "X02DA-ES1-SMP1"
    AA1_AXIS_NAME = "ROTY"
    # Drive data collection
    # task = aa1Tasks(AA1_IOC_NAME + ":TASK:", name="tsk")
    # task.wait_for_connection()
    # task.describe()
    # ddc = aa1AxisDriveDataCollection("X02DA-ES1-SMP1:ROTY:DDC:", name="ddc")
    # ddc.wait_for_connection()
    globb = aa1GlobalVariableBindings(AA1_IOC_NAME + ":VAR:", name="globb")
    globb.wait_for_connection()
    globb.describe()
    mot = EpicsMotor(AA1_IOC_NAME + ":" + AA1_AXIS_NAME, name="x")
    mot.wait_for_connection()
