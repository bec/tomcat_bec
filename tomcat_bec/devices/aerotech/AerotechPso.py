# -*- coding: utf-8 -*-
"""
Ophyd device for the Aerotech Automation1 IOC's axis-specific position
synchronized output (PSO) interface.

@author: mohacsi_i
"""
from time import sleep
import numpy as np
from ophyd import Component, EpicsSignal, EpicsSignalRO, Kind
from ophyd.status import DeviceStatus

from ophyd_devices.interfaces.base_classes.psi_detector_base import PSIDetectorBase as PSIDeviceBase
from ophyd_devices.interfaces.base_classes.psi_detector_base import (
    CustomDetectorMixin as CustomDeviceMixin,
)
from bec_lib import bec_logger
logger = bec_logger.logger


class AerotechPsoDistanceMixin(CustomDeviceMixin):
    """Mixin class for self-configuration and staging
    """
    # parent : aa1Tasks
    def on_stage(self) -> None:
        """Configuration and staging
        
        NOTE: Scans don't have to fully configure the device, that can be done
            manually outside. However we expect that the device is disabled
            when not in use. I.e. this method is not expected to be called when
            PSO is not needed or when it'd conflict with other devices.
        """
        # Fish out configuration from scaninfo (does not need to be full configuration)
        d = {}
        if "kwargs" in self.parent.scaninfo.scan_msg.info:
            scanargs = self.parent.scaninfo.scan_msg.info["kwargs"]
            if "pso_distance" in scanargs:
                d["pso_distance"] = scanargs["pso_distance"]
            if "pso_wavemode" in scanargs:
                d["pso_wavemode"] = scanargs["pso_wavemode"]
            if "pso_w_pulse" in scanargs:
                d["pso_w_pulse"] = scanargs["pso_w_pulse"]
            if "pso_t_pulse" in scanargs:
                d["pso_t_pulse"] = scanargs["pso_t_pulse"]
            if "pso_n_pulse" in scanargs:
                d["pso_n_pulse"] = scanargs["pso_n_pulse"]

        # Perform bluesky-style configuration
        if len(d) > 0:
            logger.info(f"[{self.parent.name}] Configuring with:\n{d}")
            self.parent.configure(d=d)

        # Stage the PSO distance module
        self.parent.bluestage()

    def on_unstage(self):
        """Standard bluesky unstage"""
        # Ensure output is set to low
        # if self.parent.output.value:
        #     self.parent.toggle()
        # Turn off window mode
        self.parent.winOutput.set("Off").wait()
        self.parent.winEvents.set("Off").wait()
        # Turn off distance mode
        self.parent.dstEventsEna.set("Off").wait()
        self.parent.dstCounterEna.set("Off").wait()
        # Disable output
        self.parent.outSource.set("None").wait()
        # Sleep for one poll period
        sleep(0.2)

    def on_trigger(self) -> None | DeviceStatus:
        """Fire a single PSO event (i.e. manual software trigger)"""
        # Only trigger if distance was set to invalid
        logger.warning(f"[{self.parent.name}] Triggerin...")
        if self.parent.dstDistanceVal.get() == 0:
            status = self.parent._eventSingle.set(1, settle_time=0.1)
            return status


class aa1AxisPsoBase(PSIDeviceBase):
    """Position Sensitive Output - Base class

    This class provides convenience wrappers around the Aerotech IOC's PSO
    functionality. As a base class, it's just a collection of PVs without
    significant logic (that should be implemented in the child classes).
    It uses event-waveform concept to produce signals on the configured
    output pin: a specified position based event will trigger the generation
    of a waveform on the oputput that can be either used as exposure enable,
    as individual trigger or as a series of triggers per each event.
    As a first approach, the module follows a simple pipeline structure:
        Genrator --> Event --> Waveform --> Output

    Specific operation modes should be implemented in child classes.
    """

    # ########################################################################
    # General module status
    status = Component(EpicsSignalRO, "STATUS", auto_monitor=True, kind=Kind.normal)
    output = Component(EpicsSignalRO, "OUTPUT-RBV", auto_monitor=True, kind=Kind.normal)
    address = Component(EpicsSignalRO, "ARRAY-ADDR", kind=Kind.config)
    _eventSingle = Component(EpicsSignal, "EVENT:SINGLE", put_complete=True, kind=Kind.omitted)

    # ########################################################################
    # PSO Distance event module
    posInput = Component(EpicsSignalRO, "DIST:INPUT", kind=Kind.config)
    dstEventsEna = Component(EpicsSignal, "DIST:EVENTS", put_complete=True, kind=Kind.config)
    dstCounterEna = Component(EpicsSignal, "DIST:COUNTER", put_complete=True, kind=Kind.omitted)
    dstCounterVal = Component(EpicsSignalRO, "DIST:CTR0_RBV", auto_monitor=True, kind=Kind.normal)
    dstDistanceVal = Component(
        EpicsSignalRO, "DIST:DISTANCE_RBV", auto_monitor=True, kind=Kind.normal
    )
    dstArrayIdx = Component(EpicsSignalRO, "DIST:IDX_RBV", auto_monitor=True, kind=Kind.normal)
    dstArrayDepleted = Component(
        EpicsSignalRO, "DIST:DEPLETED-RBV", auto_monitor=True, kind=Kind.normal
    )

    dstDirection = Component(EpicsSignal, "DIST:EVENTDIR", put_complete=True, kind=Kind.omitted)
    dstDistance = Component(EpicsSignal, "DIST:DISTANCE", put_complete=True, kind=Kind.normal)
    dstDistanceArr = Component(EpicsSignal, "DIST:DISTANCES", put_complete=True, kind=Kind.omitted)
    dstArrayRearm = Component(EpicsSignal, "DIST:REARM-ARRAY", put_complete=True, kind=Kind.omitted)

    # ########################################################################
    # PSO Window event module
    winEvents = Component(EpicsSignal, "WINDOW:EVENTS", put_complete=True, kind=Kind.config)
    winOutput = Component(EpicsSignal, "WINDOW0:OUTPUT", put_complete=True, kind=Kind.config)
    winInput = Component(EpicsSignalRO, "WINDOW0:INPUT", auto_monitor=True, kind=Kind.config)
    winCounter = Component(EpicsSignal, "WINDOW0:COUNTER", put_complete=True, kind=Kind.omitted)
    winBoundsArr = Component(EpicsSignal, "WINDOW0:BOUNDS", put_complete=True, kind=Kind.omitted)
    _winLower = Component(EpicsSignal, "WINDOW0:LOWER", put_complete=True, kind=Kind.config)
    _winUpper = Component(EpicsSignal, "WINDOW0:UPPER", put_complete=True, kind=Kind.config)
    winArrayIdx = Component(EpicsSignalRO, "WINDOW0:IDX_NXT", auto_monitor=True, kind=Kind.normal)
    winArrayDepleted = Component(
        EpicsSignalRO, "WINDOW0:DEPLETED-RBV", auto_monitor=True, kind=Kind.normal
    )

    # ########################################################################
    # PSO waveform module
    waveEnable = Component(EpicsSignal, "WAVE:ENABLE", put_complete=True, kind=Kind.config)
    waveMode = Component(EpicsSignal, "WAVE:MODE", put_complete=True, kind=Kind.config)
    # waveDelay = Component(EpicsSignal, "WAVE:DELAY", put_complete=True, kind=Kind.omitted)

    # PSO waveform pulse output
    # pulseTrunc = Component(EpicsSignal, "WAVE:PULSE:TRUNC", put_complete=True, kind=Kind.omitted)
    pulseOnTime = Component(EpicsSignal, "WAVE:PULSE:ONTIME", put_complete=True, kind=Kind.config)
    pulseWindow = Component(EpicsSignal, "WAVE:PULSE:PERIOD", put_complete=True, kind=Kind.config)
    pulseCount = Component(EpicsSignal, "WAVE:PULSE:COUNT", put_complete=True, kind=Kind.config)
    pulseApply = Component(EpicsSignal, "WAVE:PULSE:APPLY", put_complete=True, kind=Kind.omitted)

    # ########################################################################
    # PSO output module
    outPin = Component(EpicsSignalRO, "PIN", auto_monitor=True, kind=Kind.config)
    outSource = Component(EpicsSignal, "SOURCE", put_complete=True, kind=Kind.config)

    def trigger(self, settle_time=0.1) -> DeviceStatus:
        """Fire a single PSO event (i.e. manual software trigger)"""
        self._eventSingle.set(1, settle_time=settle_time).wait()
        status = DeviceStatus(self)
        status.set_finished()
        return status

    def toggle(self):
        """Toggle waveform outup"""
        orig_wave_mode = self.waveMode.get()
        self.waveMode.set("Toggle").wait()
        self.trigger(0.1)
        self.waveMode.set(orig_wave_mode).wait()

    def configure(self, d: dict):
        """Configure the emitted waveform"""
        wmode = d.get("pso_wavemode", "pulsed")
        t_pulse = d.get("pso_t_pulse", 100)
        w_pulse = d.get("pso_w_pulse", 200)
        n_pulse = d.get("pso_n_pulse", 1)

        # Configure the pulsed/toggled waveform
        if wmode in ["toggle", "toggled"]:
            # Switching to simple toggle mode
            self.waveEnable.set("On").wait()
            self.waveMode.set("Toggle").wait()
        elif wmode in ["pulse", "pulsed"]:
            # Switching to pulsed mode
            self.waveEnable.set("On").wait()
            self.waveMode.set("Pulse").wait()
            # Setting pulse shape
            self.pulseWindow.set(w_pulse).wait()
            self.pulseOnTime.set(t_pulse).wait()
            self.pulseCount.set(n_pulse).wait()
            # Commiting configuration
            self.pulseApply.set(1).wait()
            # Enabling PSO waveform outputs
            self.waveEnable.set("On").wait()
        elif wmode in ["output", "flag"]:
            self.waveEnable.set("Off").wait()
        else:
            raise RuntimeError(f"Unsupported window mode: {wmode}")

        # Set PSO output data source
        # FIXME : This is essentially staging...
        if wmode in ["toggle", "toggled", "pulse", "pulsed"]:
            self.outSource.set("Waveform").wait()
        elif wmode in ["output", "flag"]:
            self.outSource.set("Window").wait()


class aa1AxisPsoDistance(aa1AxisPsoBase):
    """Position Sensitive Output - Distance mode

    This class provides convenience wrappers around the Aerotech API's PSO functionality in
    distance mode. It uses event-waveform concept to produce signals on the output. A position
    based event will trigger the generation of a waveform pattern on the oputput. So the
    simplified pipeline structure is:
        Genrator (distance) --> Event --> Waveform --> Output

    NOTE: PSO module has 32 bit counters, distances are relative so they're more resistant to
    overflows.

    Examples:
    ---------
    ```
        # Configure the PSO to raise an 'enable' signal for 180 degrees
        pso = aa1AxisPsoDistance(AA1_IOC_NAME+":ROTY:PSO:", name="pso")
        pso.wait_for_connection()
        pso.configure(d={'pso_distance': [180, 0.1], 'pso_wavemode': "toggle"})
        pso.kickoff().wait()

        # Configure the PSO to emmit 5 triggers every 1.8 degrees
        pso = aa1AxisPsoDistance(AA1_IOC_NAME+":ROTY:PSO:", name="pso")
        pso.wait_for_connection()
        pso.configure(d={'pso_distance': 1.8, 'pso_wavemode': "pulsed", 'pso_n_pulse': 5})
        pso.kickoff().wait()
    ```
    """

    custom_prepare_cls = AerotechPsoDistanceMixin
    USER_ACCESS = ["configure", "prepare", "toggle"]
    _distance_value = None

    # ########################################################################
    # PSO high level interface
    def configure(self, d: dict = {}) -> tuple:
        """Simplified configuration interface to access the most common
        functionality for distance mode PSO.

        :param pso_distance: Distance or array of distances between subsequent trigger points.
        :param pso_wavemode: Waveform mode configuration, usually pulsed/toggled (default: pulsed).
        :param pso_t_pulse : trigger high duration in pulsed mode (default: 100 us)
        :param pso_w_pulse : trigger hold-off time in pulsed mode (default: 200 us)
        :param pso_n_pulse : trigger number of pulses in pulsed mode (default: 1)

        """
        pso_distance = d.get("pso_distance", 0)
        pso_wavemode = d.get("pso_wavemode", "pulsed")

        # Validate input parameters
        if pso_wavemode is not None and pso_wavemode not in [
            "pulse",
            "pulsed",
            "toggle",
            "toggled",
        ]:
            raise RuntimeError(f"Unsupported distace triggering mode: {pso_wavemode}")

        old = self.read_configuration()
        # Disable everything
        self.winEvents.set("Off").wait()
        self.dstCounterEna.set("Off").wait()
        self.dstEventsEna.set("Off").wait()

        # Configure distance generator (also resets counter to 0)
        self._distance_value = pso_distance
        if pso_distance is not None:
            if isinstance(pso_distance, (float, int)):
                self.dstDistance.set(pso_distance).wait()
            elif isinstance(pso_distance, (np.ndarray, list, tuple)):
                self.dstDistanceArr.set(pso_distance).wait()

        # Configure the pulsed/toggled waveform
        super().configure(d)

        new = self.read_configuration()
        logger.info(f"[{self.name}] PSO configured to {pso_wavemode} mode")
        return (old, new)

    def bluestage(self) -> None:
        """Bluesky style stage"""
        # Stage the PSO distance module and zero counter
        if isinstance(self._distance_value, (np.ndarray, list, tuple)):
            self.dstArrayRearm.set(1).wait()
        # Wait for polling
        sleep(0.5)
        # Start monitoring the counters if distance is valid
        if self.dstDistanceVal.get() > 0:
            self.dstEventsEna.set("On").wait()
            self.dstCounterEna.set("On").wait()
