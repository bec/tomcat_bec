# -*- coding: utf-8 -*-
"""
Ophyd device for the Aerotech Automation1 IOC's axis-specific synchronized 
drive data collection (DDC) interface.

@author: mohacsi_i
"""
import time
from collections import OrderedDict

from ophyd import Component, EpicsSignal, EpicsSignalRO, Kind
from ophyd.status import SubscriptionStatus

from ophyd_devices.interfaces.base_classes.psi_detector_base import PSIDetectorBase as PSIDeviceBase
from ophyd_devices.interfaces.base_classes.psi_detector_base import (
    CustomDetectorMixin as CustomDeviceMixin,
)
from bec_lib import bec_logger

logger = bec_logger.logger


class AerotechDriveDataCollectionMixin(CustomDeviceMixin):
    """Mixin class for self-configuration and staging
    
    NOTE: scripted scans start drive data collection internally
    """
    # parent : aa1Tasks
    def on_stage(self) -> None:
        """Configuration and staging"""

        # Fish out configuration from scaninfo (does not need to be full configuration)
        d = {}
        if "kwargs" in self.parent.scaninfo.scan_msg.info:
            scanargs = self.parent.scaninfo.scan_msg.info["kwargs"]
            # NOTE: Scans don't have to fully configure the device
            if "ddc_trigger" in scanargs:
                d["ddc_trigger"] = scanargs["ddc_trigger"]
            if "ddc_num_points" in scanargs:
                d["num_points_total"] = scanargs["ddc_num_points"]
            else:
                # Try to figure out number of points
                num_points = 1
                points_valid = False
                if "steps" in scanargs and scanargs['steps'] is not None:
                    num_points *= scanargs["steps"]
                    points_valid = True
                elif "exp_burst" in scanargs and scanargs['exp_burst'] is not None:
                    num_points *= scanargs["exp_burst"]
                    points_valid = True
                elif "repeats" in scanargs and scanargs['repeats'] is not None:
                    num_points *= scanargs["repeats"]
                    points_valid = True
                if points_valid:
                    d["num_points_total"] = num_points

        # Perform bluesky-style configuration
        if len(d) > 0:
            logger.warning(f"[{self.parent.name}] Configuring with:\n{d}")
            self.parent.configure(d=d)

        # Stage the data collection if not in internally launced mode
        # NOTE: Scripted scans start acquiring from the scrits
        if self.parent.scaninfo.scan_type not in ("script", "scripted"):
            self.parent.bluestage()

    def on_unstage(self):
        """Standard bluesky unstage"""
        self.parent._switch.set("Stop", settle_time=0.2).wait()


class aa1AxisDriveDataCollection(PSIDeviceBase):
    """Axis data collection

    This class provides convenience wrappers around the Aerotech API's axis
    specific data collection functionality. This module allows to record
    internal or external hardware synchronized signals with up to 200 kHz.

    The default configuration is using a fixed memory mapping allowing up to
    1 million recorded data points on an XC4e (this depends on controller).

    Usage:
        # Configure the DDC with default internal triggers
        ddc = aa1AxisPsoDistance(AA1_IOC_NAME+":ROTY:DDC:", name="ddc")
        ddc.wait_for_connection()
        ddc.configure(d={'num_points_total': 5000})
        ddc.stage()
        ...
        ret = yield from ddc.collect()


    NOTE: Expected behavior is that the device is disabled when not in use,
          i.e. there's avtive enable/disable management.
    """

    # ########################################################################
    # General module status
    state = Component(EpicsSignalRO, "STATE", auto_monitor=True, kind=Kind.normal)
    nsamples_rbv = Component(EpicsSignalRO, "SAMPLES_RBV", auto_monitor=True, kind=Kind.hinted)
    _switch = Component(EpicsSignal, "ACQUIRE", put_complete=True, kind=Kind.omitted)
    _input0 = Component(EpicsSignal, "INPUT0", put_complete=True, kind=Kind.config)
    _input1 = Component(EpicsSignal, "INPUT1", put_complete=True, kind=Kind.config)
    _trigger = Component(EpicsSignal, "TRIGGER", put_complete=True, kind=Kind.config)

    npoints = Component(EpicsSignal, "NPOINTS", put_complete=True, kind=Kind.config)
    _readback0 = Component(EpicsSignal, "AREAD0", kind=Kind.omitted)
    _readstatus0 = Component(EpicsSignalRO, "AREAD0_RBV", auto_monitor=True, kind=Kind.omitted)
    _readback1 = Component(EpicsSignal, "AREAD1", kind=Kind.omitted)
    _readstatus1 = Component(EpicsSignalRO, "AREAD1_RBV", auto_monitor=True, kind=Kind.omitted)

    _buffer0 = Component(EpicsSignalRO, "BUFFER0", auto_monitor=True, kind=Kind.normal)
    _buffer1 = Component(EpicsSignalRO, "BUFFER1", auto_monitor=True, kind=Kind.normal)

    custom_prepare_cls = AerotechDriveDataCollectionMixin
    USER_ACCESS = ["configure", "reset"]

    def configure(self, d: dict = None) -> tuple:
        """Configure data capture

        Configures the hardware synchronized drive data capture (DDC) on an
        Automation1 axis wit up to 200kHz external input frequency. The
        Aerotech API allows the simultaneous capture of two signals into the
        limited amount of local DriveArray (2-16 MB/axis).
        """
        old = self.read_configuration()

        if d is not None:
            if "num_points_total" in d:
                self.npoints.set(d["num_points_total"]).wait()
            if "ddc_trigger" in d:
                self._trigger.set(d['ddc_trigger']).wait()
            if "ddc_source0" in d:
                self._input0.set(d['ddc_source0']).wait()
            if "ddc_source1" in d:
                self._input1.set(d['ddc_source1']).wait()

        # Reset incremental readback
        self._switch.set("ResetRB", settle_time=0.1).wait()
        new = self.read_configuration()
        return (old, new)

    def bluestage(self) -> None:
        """Bluesky-style stage"""
        self._switch.set("Start", settle_time=0.2).wait()

    def reset(self):
        """Reset incremental readback"""
        self._switch.set("ResetRB", settle_time=0.1).wait()

    def _collect(self, index=0):
        """Force a readback of the data buffer

        Note that there's a weird behaviour in ophyd that it issues an
        initial update event with the initial value but 0 timestamp. Theese
        old_values are invalid and must be filtered out.
        """

        # Define wait until the busy flag goes down (excluding initial update)
        timestamp_ = 0

        def neg_edge(*args, old_value, value, timestamp, **kwargs):
            nonlocal timestamp_
            result = False if (timestamp_ == 0) else (old_value == 1 and value == 0)
            timestamp_ = timestamp
            return result

        if index == 0:
            status = SubscriptionStatus(self._readstatus0, neg_edge, settle_time=0.5)
            self._readback0.set(1).wait()
        elif index == 1:
            status = SubscriptionStatus(self._readstatus1, neg_edge, settle_time=0.5)
            self._readback1.set(1).wait()

        # Start asynchronous readback
        status.wait()
        return status

    def describe_collect(self) -> OrderedDict:
        """Describes collected array format according to JSONschema
        """
        ret = OrderedDict()
        ret["buffer0"] = {
            "source": "internal",
            "dtype": "array",
            "shape": [],
            "units": "",
            "lower_ctrl_limit": 0,
            "upper_ctrl_limit": 0,
        }
        ret["buffer1"] = {
            "source": "internal",
            "dtype": "array",
            "shape": [],
            "units": "",
            "lower_ctrl_limit": 0,
            "upper_ctrl_limit": 0,
        }
        return {self.name: ret}

    def collect(self) -> OrderedDict:
        """Standard collect method

        Note: bluesky expects a generator, while BEC expects a direct return
        """
        # FIXME: check out the _send_data_to_bec method
        # FIXME : For constant streaming, chck the pandabox branch
        # Complete will be called for cleanup so this can go there
        self._collect(0).wait()
        self._collect(1).wait()

        b0 = self._buffer0.value
        b1 = self._buffer1.value
        ret = OrderedDict()
        ret["timestamps"] = {"buffer0": time.time(), "buffer1": time.time()}
        ret["data"] = {"buffer0": b0, "buffer1": b1}
        return ret
