# -*- coding: utf-8 -*-
"""
Ophyd device for the Aerotech Automation1 IOC's controller's task management
interface.

@author: mohacsi_i
"""
from time import sleep
from ophyd import Component, EpicsSignal, EpicsSignalRO, Kind
from ophyd.status import DeviceStatus, SubscriptionStatus

from ophyd_devices.interfaces.base_classes.psi_detector_base import PSIDetectorBase as PSIDeviceBase
from ophyd_devices.interfaces.base_classes.psi_detector_base import (
    CustomDetectorMixin as CustomDeviceMixin,
)
from bec_lib import bec_logger

logger = bec_logger.logger


class AerotechTasksMixin(CustomDeviceMixin):
    """Mixin class for self-configuration and staging
    """
    # parent : aa1Tasks
    def on_stage(self) -> None:
        """Configuration and staging

        In the BEC model ophyd devices must fish out their own configuration from the 'scaninfo'.
        I.e. they need to know which parameters are relevant for them at each scan.

        NOTE: Scans don't have to fully configure the device, that can be done
            manually outside. However we expect that the device is disabled
            when not in use. I.e. this method is not expected to be called when
            PSO is not needed or when it'd conflict with other devices.
        """
        # logger.warning(self.parent.scaninfo.scan_msg.info['kwargs'].keys())

        # Fish out our configuration from scaninfo (via explicit or generic addressing)
        d = {}
        if "kwargs" in self.parent.scaninfo.scan_msg.info:
            scanargs = self.parent.scaninfo.scan_msg.info["kwargs"]
            if self.parent.scaninfo.scan_type in ("script", "scripted"):
                # NOTE: Scans don't have to fully configure the device
                if "script_text" in scanargs and scanargs["script_text"] is not None:
                    d["script_text"] = scanargs["script_text"]
                if "script_file" in scanargs and scanargs["script_file"] is not None:
                    d["script_file"] = scanargs["script_file"]
                if "script_task" in scanargs and scanargs["script_task"] is not None:
                    d["script_task"] = scanargs["script_task"]

        # Perform bluesky-style configuration
        if len(d) > 0:
            logger.warning(f"[{self.parent.name}] Configuring with:\n{d}")
            self.parent.configure(d=d)

        # The actual staging
        self.parent.bluestage()

    def on_unstage(self):
        """Stop the currently selected task"""
        self.parent.switch.set("Stop").wait()

    def on_stop(self):
        """Stop the currently selected task"""
        self.parent.switch.set("Stop").wait()


class aa1Tasks(PSIDeviceBase):
    """Task management API

    The place to manage tasks and AeroScript user files on the controller.
    You can read/write/compile/execute AeroScript files and also retrieve
    saved data files from the controller. It will also work around an ophyd
    bug that swallows failures.

    Execution does not require to store the script in a file, it will compile
    it and run it directly on a certain thread. But there's no way to
    retrieve the source code.

    Write a text into a file on the aerotech controller and execute it with kickoff.
    '''
    script="var $axis as axis = ROTY\\nMoveAbsolute($axis, 42, 90)"
    tsk = aa1Tasks(AA1_IOC_NAME+":TASK:", name="tsk")
    tsk.wait_for_connection()
    tsk.configure({'text': script, 'filename': "foobar.ascript", 'taskIndex': 4})
    tsk.kickoff().wait()
    '''

    Just execute an ascript file already on the aerotech controller.
    '''
    tsk = aa1Tasks(AA1_IOC_NAME+":TASK:", name="tsk")
    tsk.wait_for_connection()
    tsk.configure({'filename': "foobar.ascript", 'taskIndex': 4})
    tsk.kickoff().wait()
    '''

    """

    custom_prepare_cls = AerotechTasksMixin

    _failure = Component(EpicsSignalRO, "FAILURE", auto_monitor=True, kind=Kind.normal)
    errStatus = Component(EpicsSignalRO, "ERRW", auto_monitor=True, kind=Kind.normal)
    warnStatus = Component(EpicsSignalRO, "WARNW", auto_monitor=True, kind=Kind.normal)
    taskStates = Component(EpicsSignalRO, "STATES-RBV", auto_monitor=True, kind=Kind.normal)
    taskIndex = Component(EpicsSignal, "TASKIDX", kind=Kind.config, put_complete=True)
    switch = Component(EpicsSignal, "SWITCH", kind=Kind.config, put_complete=True)
    _execute = Component(EpicsSignal, "EXECUTE", string=True, kind=Kind.config, put_complete=True)
    _executeMode = Component(EpicsSignal, "EXECUTE-MODE", kind=Kind.config, put_complete=True)
    _executeReply = Component(EpicsSignalRO, "EXECUTE_RBV", string=True, auto_monitor=True)

    fileName = Component(EpicsSignal, "FILENAME", string=True, kind=Kind.omitted, put_complete=True)
    # _fileRead = Component(EpicsPassiveRO, "FILEREAD", string=True, kind=Kind.omitted)
    _fileWrite = Component(
        EpicsSignal, "FILEWRITE", string=True, kind=Kind.omitted, put_complete=True
    )

    def configure(self, d: dict) -> tuple:
        """Configuration interface for flying"""
        # Common operations
        old = self.read_configuration()
        self.switch.set("Reset").wait()
        # Check what we got
        if "script_task" in d:
            if d['script_task'] < 3 or d['script_task'] > 21:
                raise RuntimeError(f"Invalid task index: {d['script_task']}")
            self.taskIndex.set(d['script_task']).wait()
        if "script_file" in d:
            self.fileName.set(d["script_file"]).wait()
        if "script_text" in d:
            # Compile text for syntax checking
            # NOTE: This will load to 'script_file'
            self._fileWrite.set(d['script_text'], settle_time=0.2).wait()
            self.switch.set("Load").wait()
            # Check the result of load
            if self._failure.value:
                raise RuntimeError("Failed to load script, perhaps a syntax error?")

        new = self.read_configuration()
        return (old, new)

    def bluestage(self) -> None:
        """Bluesky style stage"""
        if self.taskIndex.get() in (0, 1, 2):
            logger.error(f"[{self.name}] Launching AeroScript on system task. Daring today are we?")
        # Launch and check success
        status = self.switch.set("Run", settle_time=0.2)
        status.wait()
        if self._failure.value:
            raise RuntimeError("Failed to kick off task, please check the Aerotech IOC")
        return status

    ##########################################################################
    # Bluesky flyer interface
    def complete(self) -> DeviceStatus:
        """Wait for a RUNNING task"""
        timestamp_ = 0
        task_idx = int(self.taskIndex.get())

        def not_running(*args, value, timestamp, **kwargs):
            nonlocal timestamp_
            result = value[task_idx] not in ["Running", 4]
            timestamp_ = timestamp
            return result

        # Subscribe and wait for update
        status = SubscriptionStatus(self.taskStates, not_running, settle_time=0.5)
        return status
